<?php

class User extends BaseObject {
	
	private $_id;
	
	public $firstName;
	public $lastName;
	public $email;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($userID) {
        $instance = new self();
        $instance->_id = $userID;
        $instance->loadById();
        return $instance;
    }
	
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM users WHERE userID = :userID');
        $sth->execute(array(':userID' => $this->_id));	
    	$record = $sth -> fetch();
        $this->fill($record);
    }

	
    protected function fill(array $row){
    	$this -> firstName = $row['firstName'];
		$this -> lastName = $row['lastName'];
		$this -> email = $row['email'];
    }

}