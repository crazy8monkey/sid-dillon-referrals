<?php

class ReferralMember extends BaseObject {
	
	private $_referralClubIDs;
	
	private $_id;
	
	public $store;
	public $firstName;
	public $lastName;
	public $email;
	public $Phone;
	public $address;
	public $city;
	public $State;
	public $zip;
	public $userName;
	public $password;
	public $agreement;
	public $ClubID;
	public $SecurityToken;
	
	private $salt;
	
	public $memberActiveStatus;
	public $lastInsertedID;
	
	public $deactivatedStatus;
	
	public $SidDillonRelation;
	
    public function __sleep() {
        parent::__sleep();
		 return array('_id');
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($referralMemberID) {
        $instance = new self();
        $instance->_id = $referralMemberID;
        $instance->loadById();
        return $instance;
    }
	
	public static function WithMemberID($MemberID) {
        $instance = new self();
        $instance-> ClubID = $MemberID;
        $instance-> loadById();
        return $instance;
    }
	
	
    protected function loadByID() {
    	if(isset($this->_id)) {
    		$sth = $this -> db -> prepare('SELECT * FROM referralmembers WHERE referralMemberID = :referralMemberID');
        	$sth->execute(array(':referralMemberID' => $this->_id));	
    	} else if(isset($this->ClubID)) {
        	$sth = $this -> db -> prepare('SELECT * FROM referralmembers WHERE ClubID = :ClubID');
        	$sth->execute(array(':ClubID' => $this->ClubID));
		}
		
        $record = $sth -> fetch();
        $this->fill($record);
    }

	
    protected function fill(array $row){
    	$this -> firstName = $row['memberFirstName'];
		$this -> lastName = $row['memberLastName'];
		$this -> email = $row['memberEmail'];
		$this -> Phone = $row['phone'];
		$this -> address = $row['address'];
		$this -> city = $row['city'];
		$this -> State = $row['state'];
		$this -> zip = $row['zip'];
		$this -> userName = $row['userName'];
		$this -> ClubID = $row['ClubID'];
    	$this -> salt = $row['salt'];
		$this -> password = $row['password'];
		$this -> SecurityToken = $row['securityToken'];
		$this -> SidDillonRelation = $row['isSidDillonRelation'];
		$this -> memberActiveStatus = $row['ActiveState'];
		$this -> deactivatedStatus = $row['DeactiveStatusReason'];
    }

	public function GetMemberID() {
		return $this->_id;
	}


	public function Validate() {
		
		if(isset($this -> _id)) {
			$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = :email AND referralMemberID <> :referralMemberID');
			$emailCheck->execute(array(':email' => $this -> email, ':referralMemberID' => $this -> _id));
			
			$userNameCheck = $this->db->prepare('SELECT userName FROM referralmembers WHERE userName = :userName AND referralMemberID <> :referralMemberID');
		    $userNameCheck->execute(array(':userName' => $this -> userName, ':referralMemberID' => $this -> _id));	
				
				

		} else {
			$emailCheck = $this->db->prepare('SELECT memberEmail FROM referralmembers WHERE memberEmail = ?');
			$emailCheck->execute(array($this -> email));
			
			$userNameCheck = $this->db->prepare('SELECT userName FROM referralmembers WHERE userName = ?');
		    $userNameCheck->execute(array($this -> userName));			
		}
		
		$validationErrors = array();
		
		
		//empty first name
		if($this -> validate -> emptyInput($this -> firstName)) {
			array_push($validationErrors, array("inputID" => 2,
												"inputType" => "First Name",
												'errorMessage' => 'Required'));
		} else if($this -> validate -> notUsingLetters($this -> firstName)) {
			array_push($validationErrors, array("inputID" => 2,
												"inputType" => "First Name",
												'errorMessage' => 'Only Letters'));
		}
		
		//empty last name
		if($this -> validate -> emptyInput($this -> lastName)) {
			array_push($validationErrors, array("inputID" => 3,
												"inputType" => "Last Name",
												"errorMessage" => "Required"));
		}
		
		//empty Email
		if($this -> validate -> emptyInput($this -> email)) {
			array_push($validationErrors, array("inputID" => 4,
												"inputType" => "Email",
												"errorMessage" => "Required"));
		//incorrect email format
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			array_push($validationErrors, array("inputID" => 4,
												"inputType" => "Email",
												"errorMessage" => "Incorrect Email Format"));					
		//duplicate emails										
		} else if(count($emailCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 4,
												"inputType" => "Email",
												"errorMessage" => "Email associated with a different account"));
		}
		
		//validate phone

		//empty phone
		if($this -> validate -> emptyInput($this -> Phone)) {
			array_push($validationErrors, array("inputID" => 5,
												"inputType" => "Phone",
												"errorMessage" => "Required"));
		//only letters
		} else if($this -> validate -> HasLetters($this -> Phone)) {
			array_push($validationErrors, array("inputID" => 5,
												"inputType" => "Phone",
												"errorMessage" => "Only Numbers"));
		} 
		
		
		//empty address
		if($this -> validate -> emptyInput($this -> address)) {
			array_push($validationErrors, array("inputID" => 6,
												"inputType" => "Address",
												"errorMessage" => "Required"));
		}
		
		//empty City
		if($this -> validate -> emptyInput($this -> city)) {
			array_push($validationErrors, array("inputID" => 7,
												"inputType" => "City",
												"errorMessage" => "Required"));
		}
		
		//empty State
		if($this -> validate -> emptyInput($this -> State)) {
			array_push($validationErrors, array("inputID" => 8,
												"inputType" => "State",
												"errorMessage" => "Required"));
		}
		
		//empty Zip
		if($this -> validate -> emptyInput($this -> zip)) {
			array_push($validationErrors, array("inputID" => 9,
												"inputType" => "Zip",
												"errorMessage" => "Required"));
		}
		
		//empty username
		if($this -> validate -> emptyInput($this -> userName)) {
			array_push($validationErrors, array("inputID" => 10,
												"inputType" => "Username",
												"errorMessage" => "Required"));
		//duplicate username									
		} else if(count($userNameCheck -> fetchAll())) {
			array_push($validationErrors, array("inputID" => 10,
												"inputType" => "Username",
												"errorMessage" => "Username associated with a different account"));
		}
		
		if(!isset($this -> _id)) {
			//empty Password
			if($this -> validate -> emptyInput($this->_initialPassword)) {
				array_push($validationErrors, array("inputID" => 11,
													"inputType" => "Password",
													"errorMessage" => "Required"));
			} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
				array_push($validationErrors, array("inputID" => 11,
													"inputType" => "Password",
													"errorMessage" => $this -> msg -> passwordLengthMessage("6")));
			}			
				
			//did not read agreement
			if($this -> validate -> emptyInput($this-> agreement)) {
				array_push($validationErrors, array("inputID" => 12,
													"inputType" => "Rules And Regulations",
													"errorMessage" => "You must agree with our Rules and Regulations"));
			}	
		} else {
			
			if(!empty($this->_initialPassword)) {
				//if password length is not 6 characters
				if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
					array_push($validationErrors, array("inputID" => 11,
														"inputType" => "Password",
														"errorMessage" => $this -> msg -> passwordLengthMessage("6")));
				}	
			}
				
			
		}	
		
		if(!isset($this -> SidDillonRelation)) {
			array_push($validationErrors, array("inputID" => 13,
												"inputType" => "Sid Dillon Relation",
												"errorMessage" => "Required"));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
		
		
	}
	
	

	
	public function ValidateCredentials() {
		$clubNumberIDCheck = str_replace("SD", "", $this -> ClubID);
			
			
		$clubIDCheck = $this->db->prepare("SELECT * FROM referralmembers WHERE ClubID = :ClubID");
		$clubIDCheck -> execute(array(':ClubID' => $clubNumberIDCheck));
							
		//$data = $sth->fetch();							 
		$ClubIDCheckPresent =  $clubIDCheck->rowCount();		
			
		
		if($this -> validate -> emptyInput($this -> ClubID)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Club ID"));
			return false;
		} else if(strpos($this -> ClubID, 'SD') === false) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Club ID was entered incorrectly");
			return false;
		} else if($ClubIDCheckPresent == 0) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Your Club ID does not match our records");
			return false;
		} else if($this -> validate -> emptyInput($this -> userName)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Username"));
			return false;
		} else if($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Password"));
			return false;
		} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;		
		}
		return true;
	}

	public function ValidateNewPassword() {
		if($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Password"));
			return false;
		} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> passwordLengthMessage("6"));
			return false;		
		}
		return true;
	}
	
	public function getUserClubID() {
		
		$getUser = $this -> db -> prepare("SELECT * FROM referralmembers WHERE memberEmail = :email");
		$getUser -> execute(array(":email" => $this -> email));
		$getUserObject = $getUser -> fetch();
		
		
		if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			//return false;
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Incorrect Email Format");
			//return false;					
		} else if($this -> validate -> noEmailMatch($this -> email, $getUserObject['memberEmail'])) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "This email is not associated with any account.");
			//return false;
		} else {
			$this -> json -> outputJqueryJSONObject('success', "SD". $getUserObject['ClubID']);
		}
				
	}
	
	public function SendEmail(){
		$getUser = $this -> db -> prepare("SELECT * FROM referralmembers WHERE memberEmail = :email");
		$getUser -> execute(array(":email" => $this -> email));
		$getUserObject = $getUser -> fetch();
		
		
		if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Email"));
			//return false;
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "Incorrect Email Format");
			//return false;					
		} else if($this -> validate -> noEmailMatch($this -> email, $getUserObject['memberEmail'])) {
			$this -> json -> outputJqueryJSONObject('errorMessage', "This email is not associated with any account.");
			//return false;
		} else {
			$securityToken = Hash::generateSecurityToken();
            $this->db->update('referralmembers', array('securityToken' => $securityToken), array('referralMemberID' => $getUserObject['referralMemberID']));
			
			$this -> json -> outputJqueryJSONObject('success', array("msg" =>'<strong>Authentication Successful</strong></br/> An email has been set with instructions on resetting your password',
																	 "redirect" => PATH . "login"));
			
			
			if(LIVE_SITE == true) {
	        	$resetLink = PATH . 'login/credentials/reset?token=' . $securityToken . '&u=' . $getUserObject['referralMemberID'];	
	            $email = new Email();
	            $email -> to = $this -> email;
	            $email -> subject = "Reset Your Password";
	            $email -> PasswordReset($resetLink);			
	        }
			
		}
		
		
	}
	
	
	
	
	private $_password;
    private $_initialPassword;
	
    public function SetPassword($password) {
        $this-> salt = Hash::generateSalt();
        $this->_initialPassword = $password;
        $this -> password = Hash::create('sha256', $password, $this->salt);
    }
	
	
	public function Save() {
		try {
			//if new member	
			if(!isset($this -> _id)) {
				$clubID = $this -> GenerateClubID();	
		
				$this -> db -> insert('referralmembers', array('memberFirstName' => $this -> firstName, 
															   'memberLastName' => $this -> lastName,
															   'memberEmail' => $this -> email,
															   'phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
															   'address' => $this -> address, 
															   'city' => $this -> city,
															   'state' => $this -> State,
															   'zip' => $this -> zip,
															   'ClubID' => $clubID,
															   'userName' => $this -> userName,
															   'password' => $this -> password,
															   'salt' => $this-> salt,
															   'isSidDillonRelation' => $this -> SidDillonRelation,
															   'memberStartedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
															   'memberStartedTime' => date("H:i:s", $this -> time -> NebraskaTime())));	
															   
				if(LIVE_SITE == true) {
					$content = array();
					$content['referral-id'] = $clubID;
					$content['username'] = $this -> userName;
					$content['password'] = $this->_initialPassword;
					
					$email = new Email();
					
					$email -> to = $this -> email;
	                $email -> subject = "Welcome to the Sid Dillon Referral Club";
	            	$email -> NewReferral($content);
	            }		
											
			//if saving existing	
			} else {
				
				if(!empty($this->_initialPassword)) {
					$postData = array('memberFirstName' => $this -> firstName, 
									  'memberLastName' => $this -> lastName,
									  'memberEmail' => $this -> email,
									  'phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
									  'address' => $this -> address, 
									  'city' => $this -> city,
									  'state' => $this -> State,
									  'zip' => $this -> zip,
									  'userName' => $this -> userName,
									  'password' => $this -> password,
									  'salt' => $this-> salt);	
				} else {
					$postData = array('memberFirstName' => $this -> firstName, 
									  'memberLastName' => $this -> lastName,
									  'memberEmail' => $this -> email,
									  'phone' => preg_replace("/[^0-9]/", "", $this -> Phone),
									  'address' => $this -> address, 
									  'city' => $this -> city,
									  'state' => $this -> State,
									  'zip' => $this -> zip,
									  'userName' => $this -> userName);
				}
				
				$this->db->update('referralmembers', $postData, array('referralMemberID' => $this -> _id));			
			}
				
														   
			$this -> json -> outputJqueryJSONObject('success', true);											 	
														   											   
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Referral Save Error: " . $e->getMessage();
			$TrackError -> type = "REFERRAL SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}					 
	}

	public function SavePassword() {
		try {
			$postData = array('password' => $this -> password,
							  'salt' => $this-> salt,
							  'securityToken' => '');	
			
			
			if(LIVE_SITE == true) {
				$content = array();
				$content['club-id'] = "SD" . $this -> ClubID;
				$content['username'] = $this -> userName;
				$content['password'] = $this->_initialPassword;
				
				$email = new Email();
				
				$email -> to = $this -> email;
                $email -> subject = "Sid Dillon Referral Credentials";
            	$email -> UserNewCredentials($content);
			}
				  
			
			$this->db->update('referralmembers', $postData, array('referralMemberID' => $this -> _id));
			
			$this -> json -> outputJqueryJSONObject('success', array("msg" => "Your password has been changed, you can now login with your new credentials", 
																	 "redirect" => PATH. "login"));
			
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Password Error: " . $e->getMessage();
			$TrackError -> type = "SAVE PASSWORD ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}	
	}
	
	public function SaveCredentials() {
		try {	
			$clubNumberIDCheck = str_replace("SD", "", $this -> ClubID);
				
				
			$clubIDCheck = $this->db->prepare("SELECT * FROM referralmembers WHERE ClubID = :ClubID");
			$clubIDCheck -> execute(array(':ClubID' => $clubNumberIDCheck));
								
			$GetMemberID = $clubIDCheck->fetch();							 
				
			
			
			$postData = array('userName' => $this -> userName,
	                          'password' => $this -> password,
	                          'salt' => $this-> salt);
							  
			$this->db->update('referralmembers', $postData, array('referralMemberID' => $GetMemberID['referralMemberID']));	
									
	        $this -> json -> outputJqueryJSONObject('credentialSaveSuccess', array("msg" => "<strong>Congratulations!</strong><br />Your credentials has been saved, you can now login to your profile",
	        																	   "redirect" => PATH . "login"));	
																				   
			if(LIVE_SITE == true) {
				$content = array();
				$content['club-id'] = "SD" . $GetMemberID['referralMemberID'];
				$content['username'] = $this -> userName;
				$content['password'] = $this->_initialPassword;
				
				$email = new Email();
				
				$email -> to = $GetMemberID['memberEmail'];
                $email -> subject = "Sid Dillon Referral Credentials";
            	$email -> UserNewCredentials($content);
			}																	   
																				   		
		
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Referral Save Credentials Error: " . $e->getMessage();
			$TrackError -> type = "REFERRAL SAVE CREDENTIALS ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("errorMessage", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("errorMessage", $e->getMessage());
			}
			
		}						  
	}
	
	
	public function userLogin() {
		$sth = $this->db->prepare("SELECT * FROM referralmembers WHERE userName = :username");
		$sth->execute(array(':username' => $this -> userName));
		
		$data = $sth->fetch();							 
		$count =  $sth->rowCount();
		
		if ($count > 0) {
			
			$userLogin = ReferralMember::WithID($data['referralMemberID']);
            if($userLogin -> ValidatePasswordAttempt($this -> password)) {
				if($userLogin -> memberActiveStatus == 0) {
					$this -> json -> outputJqueryJSONObject('deactivatedAccount', "Your Account has been deactivated for the followin reason(s)<br /><br />" . $userLogin -> deactivatedStatus. "<br /><br />Contact Jarrod Rohwer at (402) 505-4226");
				} else {
					// login	
					Session::init();
	            	Session::set('UserLoggedIn', true);
	            	//Session::set('user', $userLogin);	
					Session::set('user', $data['referralMemberID']);
					//redirect to main app section
					$this -> json -> outputJqueryJSONObject('redirect', PATH . "member/dashboard");		
				}	
				
					
					
				
				
			} else {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch());
			}
		} else {
			$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> wrongCredentialMatch(). " No Username");			
		}
	}
	
	private function ValidatePasswordAttempt($pass) {
        return Hash::create('sha256', $pass, $this -> salt) == $this -> password;
    }
	
	public function States() {
		return array('AL'=>'Alabama',
					 'AK'=>'Alaska', 
					 'AR'=>'Arkansas',
					 'AZ'=>'Arizona',
					 'CA'=>'California',
					 'CO'=>'Colorado',
					 'CT'=>'Connecticut',
					 'DE'=>'Delaware', 
					 'DC'=>'District of Columbia',
					 'FL'=>'Florida', 
					 'GA'=>'Georgia',
					 'HI'=>'Hawaii',
					 'IA'=>'Iowa',  
					 'ID'=>'Idaho',
					 'IL'=>'Illinois',
					 'IN'=>'Indiana', 
					 'KS'=>'Kansas', 
					 'KY'=>'Kentucky',
					 'LA'=>'Louisiana',
					 'ME'=>'Maine', 
					 'MD'=>'Maryland', 
					 'MA'=>'Massachusetts', 
					 'MI'=>'Michigan', 
					 'MN'=>'Minnesota', 
					 'MO'=>'Missouri',
					 'MS'=>'Mississippi', 
					 'MT'=>'Montana',
					 'NE'=>'Nebraska', 
					 'NM'=>'New Mexico', 
					 'NV'=>'Nevada',
					 'NH'=>'New Hampshire', 
					 'NJ'=>'New Jersey', 
					 'NY'=>'New York', 
					 'NC'=>'North Carolina', 
					 'ND'=>'North Dakota',
					 'OH'=>'Ohio', 
					 'OK'=>'Oklahoma', 
					 'OR'=>'Oregon', 
					 'PA'=>'Pennsylvania',
					 'RI'=>'Rhode Island', 
					 'SC'=>'South Carolina',
					 'SD'=>'South Dakota',
					 'TX'=>'Texas', 
					 'TN'=>'Tennessee',
					 'UT'=>'Utah',  
					 'VT'=>'Vermont', 
					 'VA'=>'Virginia',
					 'WA'=>'Washington', 
					 'WV'=>'West Virginia',
					 'WI'=>'Wisconsin',
					 'WY'=>'Wyoming');
	}

	public function ClubIDLookupForm($formAction) {
		return "<div class='WhitePopupForm' id='ClubIDLookup'>
					<div class='header'>
						Club ID Lookup
						<a href='javascript:void(0)' onclick='Globals.ClosePopups()'>
							<div class='close' style='margin-top: 18px;'>
								<i class='fa fa-times-circle' aria-hidden='true'></i>
							</div>			
						</a>
					</div>
					<div class='Content' style='padding-bottom:15px;'>" . $this -> form -> ValiationMessage("ClubIDLookupError", "loadingClubID", "ClubIDLookupMessage") . "
						Please type in your email associated with your account to retrieve your Club ID
						<form id='clubIDLookupForm' method='post' action='" . $formAction . "'>
							<div class='inputLine'>
								<div class='inputLabel'>Your Email</div>
								<input type='text' name='clubIDlookupEmail' />
							</div>
							<div class='buttonHolder'>
								<input type='submit' value='Lookup' class='redButton' style='border:none; padding: 6px 10px; margin-top: 13px;' />
							</div>
						</form>
					</div>
				</div>";
	}
	
	private function GenerateClubID() {
		//prents duplicate id numbers
		$finalIDNumber = "";	
			
		do {
		    	
		    $numbers = '0123456789';
			
			$SDNumber = substr(str_shuffle($numbers), 0, 5);
			$clubIDCheck = $this->db->prepare("SELECT * FROM referralmembers WHERE ClubID = :ClubID");
			$clubIDCheck -> execute(array(':ClubID' => $SDNumber));
							
			//$data = $sth->fetch();							 
			$duplicateIDCheck =  $clubIDCheck->rowCount();
			
			
			
			$finalIDNumber = $SDNumber;
		} while ($duplicateIDCheck > 0);
			
		return $finalIDNumber;
	}
    
	

}