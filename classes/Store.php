<?php

class Store extends BaseObject {
	
	private $_id;
	
	public $ReferralDefaultEmail;
	public $ReferralDefaultUser;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($storeID) {
        $instance = new self();
        $instance->_id = $storeID;
        $instance->loadById();
        return $instance;
    }
	
	
	
    protected function loadByID() {
    	$sth = $this -> db -> prepare('SELECT * FROM stores WHERE id = :id');
        $sth->execute(array(':id' => $this->_id));	
    	$record = $sth -> fetch();
        $this->fill($record);
    }

	
    protected function fill(array $row){
    	$this -> ReferralDefaultEmail = $row['ReferralStoreEmailDefault'];
		$this -> ReferralDefaultUser = $row['ReferralDefaultUser'];
    }

}