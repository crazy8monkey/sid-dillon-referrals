<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */
class Settings extends BaseObject {
		
	//1 = weekly to do list
	//2 = main
	public $type;
	public $summaryEmailReminder;
	public $facebookPosts;
	public $TwitterPosts;
	public $blogPosts;
	
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    private static $singletonSettings;
    private static $initialValue = 0;
    public static function GetSettings(){
        if(empty(self::$singletonSettings)){
            $s = new Settings();
            $sth = $s -> db -> prepare('SELECT * FROM settings WHERE settingsID = :settingsID');
            $sth->execute(array(':settingsID' => 1));
            $record = $sth -> fetch();
            $s->fill($record);
            self::$singletonSettings = $s;
        }
        return self::$singletonSettings;
    }
	
	protected function fill(array $row){
		$this -> summaryEmailReminder = $row['SummaryEmailReminder'];
		$this -> facebookPosts = $row['facebookPosts'];
		$this -> TwitterPosts = $row['TwitterPosts'];	
		$this -> blogPosts = $row['blogPost'];
    }
	
	public function validate() {
		
		
		if($this -> type == 1) {
			if($this -> validate -> emptyInput($this -> facebookPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Facebook Posts"));
				return false;
			} else if($this -> validate -> onlyNumbers($this -> facebookPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', "Facebook Posts: Only Numbers");
				return false;
			} else if($this -> validate -> emptyInput($this -> TwitterPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Twitter Posts"));
				return false;
			} else if($this -> validate -> onlyNumbers($this -> TwitterPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', "Twitter Posts: Only Numbers");
				return false;
			} else if($this -> validate -> emptyInput($this -> blogPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Blog Posts"));
				return false;
			} else if($this -> validate -> onlyNumbers($this -> blogPosts)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', "Blog Posts: Only Numbers");
				return false;
			}
		} else if($this -> type == 2) {
			if($this -> validate -> emptyInput($this -> summaryEmailReminder)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> isRequired("Summary Email Reminder"));
				return false;
			} else if($this -> validate -> correctEmailFormat($this -> summaryEmailReminder)) {
				$this -> json -> outputJqueryJSONObject('errorMessage', $this -> msg -> emailFormatRequiredMessage());
				return false;
			}
		}
		return true;
	}


	public function Save() {
		
		try {
			$updatedMessage = "";
				
			if($this -> type == 1) {
				$updatedMessage = "To Do list setings updated";
				$postData = array('facebookPosts' => $this -> facebookPosts, 
								  'TwitterPosts' => $this -> TwitterPosts);
			} else if($this -> type == 2) {
				$updatedMessage = "Setings updated";
				$postData = array('SummaryEmailReminder' => $this -> summaryEmailReminder);
			}
				
			$this->db->update('settings', $postData, array('settingsID' => 1));		
			$this -> json -> outputJqueryJSONObject("updatedSetings", $updatedMessage);
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = "Save Settings Error: " . $e->getMessage();
			$TrackError -> type = "SAVE Settings ERROR";
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject("errorMessage", $e->getMessage());
		}
		
	}	

}