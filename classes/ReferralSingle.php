<?php

class ReferralSingle extends BaseObject {
	
	private $_id;
	
	public $ClubID;
	public $StoreID;
	public $salesmanID;
	public $FirstName;
	public $LastName;
	public $phone;
	public $email;
	public $desiredVehichle;
	public $ExpectedPurchase;
	public $ReferralNotes;
	
	public $submittedDate;
	public $salesperson;
	public $storeName;
	public $SoldDate;
	
	public $LoggedInMemberID;
	public $LoggedInMember;
	
	public $SidDillonRelation;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function WithID($referralID) {
        $instance = new self();
        $instance->_id = $referralID;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID() {
        $sth = $this -> db -> prepare('SELECT * FROM referrals LEFT JOIN users ON referrals.salesmanUserID = users.userID LEFT JOIN stores ON referrals.storeID = stores.id WHERE referralID = :referralID');
        $sth->execute(array(':referralID' => $this->_id));
        $record = $sth -> fetch();
        $this->fill($record);
    }
		
    protected function fill(array $row){
    	$this -> submittedDate = $row['submittedDate'];
		$this -> salesperson = $row['firstName'] . " " . $row['lastName'];
		$this -> storeName = $row['name'];
		$this -> SoldDate = $row['isSoldDate'];
		$this -> ReferralNotes = $row['notes'];
    }


	public function Validate() {
		$clubNumberIDCheck = str_replace("SD", "", $this -> ClubID);
			
			
		$clubIDCheck = $this->db->prepare("SELECT * FROM referralmembers WHERE ClubID = :ClubID");
		$clubIDCheck -> execute(array(':ClubID' => $clubNumberIDCheck));
							
		//$data = $sth->fetch();							 
		$ClubIDCheckPresent =  $clubIDCheck->rowCount();
		
		$validationErrors = array();
		
		
		//empty Club ID
		if($this -> validate -> emptyInput($this -> ClubID)) {
			array_push($validationErrors, array("inputID" => 1,
												"inputType" => "Club ID",
												'errorMessage' => 'Required'));
												
		//if club id entered incorrectly										
												
		
		} else if(strpos($this -> ClubID, 'SD') === false) {
			array_push($validationErrors, array("inputID" => 1,
												"inputType" => "Club ID",
												'errorMessage' => 'Club ID was entered incorrectly'));
		//if club id doesn't exist
		} else if($ClubIDCheckPresent == 0) {
			array_push($validationErrors, array("inputID" => 1,
												"inputType" => "Club ID",
												'errorMessage' => 'Your Club ID does not match our records'));									
		} 
		
		//empty store selected		
		if($this -> validate -> emptyDropDown($this -> StoreID)) {
			array_push($validationErrors, array("inputID" => 2,
												"inputType" => "Store",
												'errorMessage' => 'Required'));
		} else {
			//empty salesman
			if($this -> salesmanID == "0") {
				array_push($validationErrors, array("inputID" => 3,
													"inputType" => "Salesman",
													'errorMessage' => 'Required'));
			}
		}
		
		//empty first name
		if($this -> validate -> emptyInput($this -> FirstName)) {
			array_push($validationErrors, array("inputID" => 4,
												"inputType" => "First Name",
												'errorMessage' => 'Required'));
		}
		
		//empty last name
		if($this -> validate -> emptyInput($this -> LastName)) {
			array_push($validationErrors, array("inputID" => 5,
												"inputType" => "Last Name",
												'errorMessage' => 'Required'));
		}
		
		//empty phone
		if($this -> validate -> emptyInput($this -> phone)) {
			array_push($validationErrors, array("inputID" => 6,
												"inputType" => "Phone",
												'errorMessage' => 'Required'));
		}
		
		//incorrect email format
		if(!empty($this -> email)) {
			if($this -> validate -> correctEmailFormat($this -> email)) {
				array_push($validationErrors, array("inputID" => 7,
													"inputType" => "Email",
													"errorMessage" => "Incorrect Email Format"));					
			}	
		}
		
		//desired vehicle
		if($this -> validate -> emptyInput($this -> desiredVehichle)) {
				array_push($validationErrors, array("inputID" => 8,
													"inputType" => "Desired Vehicle",
													"errorMessage" => "Required"));					
		}
		
		
		
		
		
		//desired vehicle
		if($this -> validate -> emptyInput($this -> ExpectedPurchase)) {
				array_push($validationErrors, array("inputID" => 9,
													"inputType" => "Expected Date of Purchase",
													"errorMessage" => "Required"));					
		} else if(isset($this -> ExpectedPurchase)) {
			
			$dateEntered = new DateTime($this -> time -> FormatExpectedDate($this -> ExpectedPurchase));
			$now = new DateTime();
			

			
			if($dateEntered < $now -> modify('-1 day')) {
			    array_push($validationErrors, array("inputID" => 9,
													"inputType" => "Expected Date of Purchase",
													"errorMessage" => "Needs to be today or a future date"));
			}	
		}
		
		if(!isset($this -> SidDillonRelation)) {
			array_push($validationErrors, array("inputID" => 10,
													"inputType" => "Sid Dillon Relation",
													"errorMessage" => "You need to verify your relation to Sid Dillon"));
		}
		
		
		if (empty($validationErrors)) {
			return true;
		} else {
			$this -> json -> outputJqueryJSONObject('ValidationErrors', $validationErrors);	
			return false;
		}
	}	
	

	
	public function Save() {
		try {
			$sidDillonRelationValue = NULL;
			
			
			$salesManIDValue = NULL;
			
			
			$storeReferralEmail = Store::WithID($this -> StoreID);
			
			if($this -> LoggedInMember == 1) {
				$member = ReferralMember::WithID($this -> LoggedInMemberID);
				
				$sidDillonRelationValue = $member -> SidDillonRelation;
			} else {
				$sidDillonRelationValue = $this -> SidDillonRelation;
			}

			if($this -> salesmanID == "other") {
				$salesManIDValue = $storeReferralEmail -> ReferralDefaultUser;
			} else {
				$salesManIDValue = $this -> salesmanID;
			}


			$this -> db -> insert('referrals', array('submittedDate' => date("Y-m-d", $this -> time -> NebraskaTime()),
													 'submittedTime' => date("H:i:s", $this -> time -> NebraskaTime()),
													 'storeID' => $this -> StoreID, 
													 'salesmanUserID' => $salesManIDValue,
													 'memberID' => str_replace("SD", "", $this -> ClubID),
													 'FirstName' => $this -> FirstName,
													 'LastName' => $this -> LastName, 
													 'referralSinglePhone' => preg_replace("/[^0-9]/", "", $this -> phone),
													 'referralSingleEmail' => $this -> email,
													 'DesiredVehicle' => $this -> desiredVehichle,
													 'expectedDatePurchase' => $this -> time -> FormatExpectedDate($this -> ExpectedPurchase),
													 'notes' => htmlentities($this -> ReferralNotes),
													 'referralSidDillonRelation' => $sidDillonRelationValue,
													 'ReferralSubmittedIP' => $_SERVER['REMOTE_ADDR']));	
														   
			if(LIVE_SITE == true) {
				$content = array();
				$email = new Email();
				
				$memberName = ReferralMember::WithMemberID(str_replace("SD", "", $this -> ClubID));
				
				
				
				$content['sd-number'] = $this -> ClubID;
				$content['member-full-name'] = $memberName -> firstName . ' ' . $memberName -> lastName;
				$content['referral-full-name'] = $this -> FirstName . ' ' . $this -> LastName;
				$content['referral-phone'] = $this -> phone;
				$content['referral-email'] = $this -> email;
				$content['referral-desired-vehicle'] = $this -> desiredVehichle;
				$content['referral-expected-purchase'] = $this -> ExpectedPurchase;
				$content['referral-notes'] = $this -> ReferralNotes;
				
				if($salesManIDValue == $storeReferralEmail -> ReferralDefaultUser) {
					$email -> to = $storeReferralEmail -> ReferralDefaultEmail;
				} else {
					$salesman = User::WithID($this -> salesmanID);
					$email -> to = $salesman -> email;	
				}
				
				$email -> subject = "New Referral";
				$email -> SaleRepNewReferral($content);
            }											   
														   
			$this -> json -> outputJqueryJSONObject('success', true);											 	
														   											   
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Referral Save Error: " . $e->getMessage();
			$TrackError -> type = "REFERRAL SAVE ERROR";
			$TrackError -> SendMessage();
			
			if(LIVE_SITE == true) {
				$this -> json -> outputJqueryJSONObject("MySqlError", SYSTEM_ERROR_MESSAGE);	
			} else {
				$this -> json -> outputJqueryJSONObject("MySqlError", $e->getMessage());
			}
			
		}					 
	}
	
	

}