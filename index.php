<?php

require 'config/paths.php';
require 'config/database.php';
require 'config/globals.php';


//use an autoloader
function __autoload($class) {

	require LIBS . $class. '.php';
	require_once CLASSES . 'init.php';
	require_once LISTS . 'init.php';
}

// Global error handler for those errors that are not caught by "try/catch". In this case we just re-throw it.
// Answer taken from http://stackoverflow.com/a/2468534/2106228
function myErrorHandler($errno, $errstr, $errfile, $errline) {
    if ( E_RECOVERABLE_ERROR===$errno ) {
        throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
    return false;
}
set_error_handler('myErrorHandler');

// Load the Bootstrap
$bootstrap = new Bootstrap();
// Optional Path Settings
//$bootstrap->setControllerPath();
//$bootstrap->setModelPath();
//$bootstrap->setDefaultFile();
//$bootstrap->setErrorFile();

$bootstrap->init();