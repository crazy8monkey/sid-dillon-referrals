<div class="whiteBackgroundContent">
	<div class="container" id="SubmitReferralForm">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Submit A Referral
				</div>
			</div>
		</div>
		
		<form id="NewReferralForm" method="post" action="<?php echo PATH ?>submit/save/referral">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger" id="validationSummaryMessageElement">
						<div id="divErrorHeader" style='font-weight:bold; margin-bottom:10px;'></div>
						<span id="validationSummaryMessageText"></span>	
					</div>
				</div>
			</div>
			<div class="SidDillonRelationContainer">
				<div class="row" id="inputID10">
					<div class="col-md-12">
						Are you a Sid Dillon Employee, or an immediate family member to a Sid Dillon employee?<span class="errorMessage"></span>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<div style="margin:10px 0px;">
							<input type="radio" value="1" name="SidDillonRelation" onchange="Globals.removeValidationMessage(10)" /><label for="SidDillonRelation">Yes</label>	
							<div style="clear:both"></div>
						</div>
						<div>
							<input type="radio" value="0" name="SidDillonRelation" onchange="Globals.removeValidationMessage(10)" /><label for="SidDillonRelation">No</label>
							<div style="clear:both"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row rowSpacing">
				<div class="col-md-12">
					<div class="inputLine" id="inputID1" style="margin-top: 0px;">
						<div class="inputLabel">Your Club ID (i.e. SD99999) *<span class="errorMessage"></span></div>
						<input type="text" name="clubID" value="<?php echo $this -> SDClubID?>" id="clubID" />
						<?php if(isset($this -> clubIDLookup)) : ?>
							<div style="margin:5px 0px;">
								<a href="javascript:void(0)" onclick="SubmitController.ClubIDLookup()" style="text-decoration:underline">Forgot your Club ID?</a>	
							</div>
						<?php endif; ?>
						
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="inputLine">
						<div class="dropDownIcon">
							<img src="<?php echo PATH ?>public/images/StoreIcon.png" />
						</div>
						<div class="dropDownContent" id="inputID2">
							<select class="DropDown" name="selectedStore" onchange="SubmitController.ShowSalesmanDropDown(this, 2)">
								<option value="0">Select Store</option>
								<?php foreach($this -> referralStores as $value) : ?>
									<option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo $this -> tooltip("Choose store your referral will be going to") ?>
							<div class="errorMessage" style="margin-left:0px;"></div>
						</div>
						<div style="clear:both"></div>
					</div>
				</div>
				<div class="col-md-6" id="SalesManSelect">
					<div class="inputLine">
						<div class="dropDownIcon">
							<img src="<?php echo PATH ?>public/images/SalesPerson.png" />
						</div>
						<div class="dropDownContent" id="inputID3">
							<select class="DropDown" id="salesManDropDown" name="salesman" onchange="Globals.removeValidationMessage(3)">
								<option value="0" id="EmptySalesmanOption">Select Salesperson</option>
							</select>
							<?php echo $this -> tooltip("Salesperson you want to send referral to, Choose Other if you are unsure") ?>
							<div class="errorMessage" style="margin-left:0px;"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row rowSpacing" style="margin-top: 50px;">
				<div class="col-md-12 sectionHeader">
					Referral Information
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID4">
						<div class="inputLabel">First Name *<span class="errorMessage"></span></div>
						<input type="text" name="firstName" onkeyup="Globals.removeValidationMessage(4)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID5">
						<div class="inputLabel">Last Name *<span class="errorMessage"></span></div>
						<input type="text" name="lastName" onkeyup="Globals.removeValidationMessage(5)" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID6">
						<div class="inputLabel">Phone *<span class="errorMessage"></span></div>
						<input type="text" id="PhoneInput" name="phoneInput" onkeyup="Globals.removeValidationMessage(6)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID7">
						<div class="inputLabel">Email<span class="errorMessage"></span></div>
						<input type="text" name="email" onkeyup="Globals.removeValidationMessage(7)" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID8">
						<div class="inputLabel">Desired Vehicle *<span class="errorMessage"></span></div>
						<input type="text" name="desiredVehicle" onkeyup="Globals.removeValidationMessage(8)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID9">
						<div class="inputLabel">Expected Date of Purchase *<span class="errorMessage"></span></div>
						<input type="text" id="ExpectedDate" name="ExpectedPurchase" onkeyup="Globals.removeValidationMessage(9)" />
					</div>
				</div>
			</div>
			
			<div class="row iconSeparator" style="margin-top: 50px;">
				<div class="col-md-12">
					<img src="<?php echo PATH ?>public/images/Notes.png" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" id="inputID10">
						<div class="inputLabel">Notes</div>
						<textarea name="referralNotes"></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<input type="submit" value="SUBMIT REFERRAL" class="redButton submitButton"  />
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
	<div class="container" id="ThankYouReferral">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Thank you
				</div>
			</div>
		</div>
		<div class="row" style='margin-bottom: 80px;'>
			<div class="col-md-12">
				Thank you for submitting a referral!  You can view your referral status by logging into your account.
			</div>
			
		</div>
	</div>
	
</div>
<?php if(isset($this -> clubIDLookup)) {
	echo $this -> clubIDLookup;
}
?>
<div class="LoadingReferral">
	<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
</div>

