<div class="Content">
	<div class="rule">
		<div class="number">Rule #1</div>
		All referrals must be made <bold>BEFORE</bold> the purchase of a vehicle through the online referral portal(referral.siddillon.com).  ie. 
		You will not recieve a referral if someone purchases a vehicle and then a referral is submitted.
		If you send a referral through your online account after they come to one of our locations, 
		you may still be able to recieve compensation but the referral <strong>must</strong> mention your name when they come in and must not have already purchased. 
		Your referral submition date and time will be used the verify this.
	</div>
	<div class="rule">
		<div class="number">Rule #2</div>
		To be paid for a successful referral, meaning your referral bought a vehicle from us, that customer must <strong>not</strong> have purchased a vehicle from Sid Dillon within the last <strong>8</strong> years.		
	</div>
	<div class="rule">
		<div class="number">Rule #3</div>
		If your sales representative is not at the dealership at the time of the referrals purchase, please instruct your referral to let the new salesperson know they were referred by you.
		Sid Dillon reserves the right to deny any referal that it deems as fraudulent or deceitful.
		Sid Dillon reserves the right to modify the terms and conditions of this program and may cancel this program at any time without notification to its members.		
	</div>
	<div class="rule" style="padding-top:15px; border-top: 1px solid #cecece;">
		<ul>
			<li>Referrals will be tracked on an annual basis. On January 1st of each year, everyone will start a new Referral Year.</li>
			<li>We may send you periodic updates to make you aware of any specials or new inventory that we may have at that time.</li>
		</ul>
	</div>
</div>

<!--<div class="row" style="border-bottom: 1px solid #f7f7f7;">
			<div class="col-md-6">
				You may also call Sid Dillon at (402) 464-6500, ask for Jonathan Clark or email at 
				jonclark@siddillon.com for more details. We welcome and appreciate your feedback.
			</div>
</div> -->