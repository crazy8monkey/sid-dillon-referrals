<div id="header" class="menu-top hidden-sm hidden-xs">
	<a class="brand-logo " href="http://www.siddillon.com/">
		<img src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/logo.png" alt="Sid Dillon">
	</a>
	<div class="header-right">
		<div id="navigation">
			<div class="navbar">
				<div class="navbar-inner">
					<div class="nav_section">
						<ul id="menu-main-menu" class="nav">
							<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3 current_page_item top-level-empty menu-item-53"><a href="http://www.siddillon.com/">Home</a></li>
							<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-54"><a href="http://www.siddillon.com/new-vehicles/">New Vehicles</a>
								<ul class="sub-menu">
									<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-55"><a href="http://www.siddillon.com/new-vehicles/">View All New Vehicles</a></li>
									<li id="menu-item-2095" class="make-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2095"><a href="http://www.siddillon.com/vehicle-makes/">View Vehicles By Make</a>
										<ul class="sub-menu">
											<li id="menu-item-1388" class="menu-half di-vehicle-icon buick-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1388"><a href="http://www.siddillon.com/new-vehicles/buick/">Buick</a></li>
											<li id="menu-item-1389" class="menu-half di-vehicle-icon cadillac-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1389"><a href="http://www.siddillon.com/new-vehicles/cadillac/">Cadillac</a></li>
											<li id="menu-item-1390" class="menu-half di-vehicle-icon chevrolet-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1390"><a href="http://www.siddillon.com/new-vehicles/chevrolet/">Chevrolet</a></li>
											<li id="menu-item-1391" class="menu-half di-vehicle-icon chrysler-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1391"><a href="http://www.siddillon.com/new-vehicles/chrysler/">Chrysler</a></li>
											<li id="menu-item-1379" class="menu-half di-vehicle-icon dodge-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1379"><a href="http://www.siddillon.com/new-vehicles/dodge/">Dodge</a></li>
											<li id="menu-item-1380" class="menu-half di-vehicle-icon ford-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1380"><a href="http://www.siddillon.com/new-vehicles/ford/">Ford</a></li>
											<li id="menu-item-1381" class="menu-half di-vehicle-icon gmc-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1381"><a href="http://www.siddillon.com/new-vehicles/gmc/">GMC</a></li>
											<li id="menu-item-1382" class="menu-half di-vehicle-icon hyundai-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1382"><a href="http://www.siddillon.com/new-vehicles/hyundai/">Hyundai</a></li>
											<li id="menu-item-1383" class="menu-half di-vehicle-icon jeep-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1383"><a href="http://www.siddillon.com/new-vehicles/jeep/">Jeep</a></li>
											<li id="menu-item-1384" class="menu-half di-vehicle-icon lincoln-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1384"><a href="http://www.siddillon.com/new-vehicles/lincoln/">Lincoln</a></li>
											<li id="menu-item-1385" class="menu-half di-vehicle-icon mazda-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1385"><a href="http://www.siddillon.com/new-vehicles/mazda/">Mazda</a></li>
											<li id="menu-item-1386" class="menu-half di-vehicle-icon nissan-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1386"><a href="http://www.siddillon.com/new-vehicles/nissan/">Nissan</a></li>
											<li id="menu-item-1387" class="menu-half di-vehicle-icon ram-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1387"><a href="http://www.siddillon.com/new-vehicles/ram/">Ram</a></li>
										</ul>
									</li>
									<li id="menu-item-2094" class="location-menu shift-up-2 menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2094"><a href="http://www.siddillon.com/vehicle-locations/">View Vehicles By Location</a>
										<ul class="sub-menu">
											<li id="menu-item-1398" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1398"><a href="http://www.siddillon.com/chevrolet-blair/new-vehicles/">Sid Dillon Chevrolet Blair</a></li>
											<li id="menu-item-1397" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1397"><a href="http://www.siddillon.com/ford-ceresco/new-vehicles/">Sid Dillon Ford Ceresco</a></li>
											<li id="menu-item-1399" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1399"><a href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/new-vehicles/">Sid Dillon Crete</a></li>
											<li id="menu-item-1394" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1394"><a href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/new-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont</a></li>
											<li id="menu-item-1393" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1393"><a href="http://www.siddillon.com/chevrolet-fremont/new-vehicles/">Sid Dillon Chevy Fremont</a></li>
											<li id="menu-item-1395" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1395"><a href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/new-vehicles/">Sid Dillon Lincoln</a></li>
											<li id="menu-item-1396" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1396"><a href="http://www.siddillon.com/chevrolet-buick-wahoo/new-vehicles/">Sid Dillon Chevrolet Buick Wahoo</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-56"><a href="http://www.siddillon.com/used-vehicles/">Used Vehicles</a>
								<ul class="sub-menu">
									<li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-60"><a href="http://www.siddillon.com/used-vehicles/">View All Used Cars</a></li>
									<li id="menu-item-1404" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-1404"><a href="http://www.siddillon.com/used-vehicles/">Used Vehicles By Location</a>
										<ul class="sub-menu">
											<li id="menu-item-1405" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1405"><a href="http://www.siddillon.com/chevrolet-blair/used-vehicles/">Sid Dillon Chevrolet Blair</a></li>
											<li id="menu-item-1406" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1406"><a href="http://www.siddillon.com/ford-ceresco/used-vehicles/">Sid Dillon Ford Ceresco</a></li>
											<li id="menu-item-1408" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1408"><a href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/used-vehicles/">Sid Dillon Crete</a></li>
											<li id="menu-item-1409" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1409"><a href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/used-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont</a></li>
											<li id="menu-item-1407" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1407"><a href="http://www.siddillon.com/chevrolet-fremont/used-vehicles/">Sid Dillon Chevrolet Fremont</a></li>
											<li id="menu-item-1410" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1410"><a href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/used-vehicles/">Sid Dillon Buick Nissan Hyundai Lincoln</a></li>
											<li id="menu-item-1411" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1411"><a href="http://www.siddillon.com/chevrolet-buick-wahoo/used-vehicles/">Sid Dillon Chevrolet Buick Wahoo</a></li>
										</ul>
									</li>
									<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-57"><a href="http://www.siddillon.com/used-vehicles/certified-pre-owned-vehicles/">Certified Pre-Owned Vehicles</a>
										<ul class="sub-menu">
											<li id="menu-item-1653" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1653"><a href="http://www.siddillon.com/chevrolet-blair/certified-pre-owned-vehicles/">Sid Dillon Chevrolet Blair Certified</a></li>
											<li id="menu-item-1652" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1652"><a href="http://www.siddillon.com/ford-ceresco/certified-pre-owned-vehicles/">Sid Dillon Ford Ceresco Certified</a></li>
											<li id="menu-item-1651" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1651"><a href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/certified-pre-owned-vehicles/">Sid Dillon Crete Certified</a></li>
											<li id="menu-item-1650" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1650"><a href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/certified-pre-owned-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont Certified</a></li>
											<li id="menu-item-1649" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1649"><a href="http://www.siddillon.com/chevrolet-fremont/certified-pre-owned-vehicles/">Sid Dillon Chevrolete Fremont Certified</a></li>
											<li id="menu-item-1648" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1648"><a href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/certified-pre-owned-vehicles/">Sid Dillon Buick Nissan Hyundai Lincoln Certified</a></li>
											<li id="menu-item-1647" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1647"><a href="http://www.siddillon.com/chevrolet-buick-wahoo/certified-pre-owned-vehicles/">Sid Dillon Chevrolet Buick Wahoo Certified</a></li>
										</ul>
									</li>
									<li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-58"><a href="http://www.siddillon.com/used-vehicles/priced-under-10k/">Priced Under 10k</a></li>
									<li id="menu-item-1402" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1402"><a href="http://www.siddillon.com/sid-dillon-value-your-trade/">Value Your Trade</a></li>
								</ul>
							</li>
<li id="menu-item-2322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2322"><a href="http://www.siddillon.com/all-vehicle-specials/">Specials</a>
<ul class="sub-menu">
	<li id="menu-item-4726" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-4726"><a href="http://www.siddillon.com/video-library/">Video Library</a></li>
</ul>
</li>
<li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-61"><a href="http://www.siddillon.com/finance/">Finance Centers</a>
<ul class="sub-menu">
	<li id="menu-item-1413" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1413"><a href="http://www.siddillon.com/finance/">Finance Centers</a></li>
	<li id="menu-item-1412" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1412"><a href="http://www.siddillon.com/finance/">Apply For Financing</a></li>
	<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-63"><a href="http://www.siddillon.com/payment-calculator/">Payment Calculator</a></li>
	<li id="menu-item-1403" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1403"><a href="http://www.siddillon.com/sid-dillon-value-your-trade/">Value Your Trade</a></li>
</ul>
</li>
<li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-69"><a href="http://www.siddillon.com/service/">Service &amp; Parts</a>
<ul class="sub-menu">
	<li id="menu-item-1433" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1433"><a href="http://www.siddillon.com/service/">Service Centers</a></li>
	<li id="menu-item-2297" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2297"><a href="http://www.siddillon.com/service/">Schedule Service</a></li>
	<li id="menu-item-1434" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1434"><a href="http://www.siddillon.com/body-shops/">Body Shops</a></li>
	<li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-71"><a href="http://www.siddillon.com/parts/">Parts Departments</a></li>
	<li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-72"><a href="http://www.siddillon.com/parts/order-parts/">Order Your Vehicle Parts</a></li>
	<li id="menu-item-1441" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1441"><a href="http://www.siddillon.com/accessories/">Accessories</a></li>
</ul>
</li>
<li id="menu-item-1456" class="right-edge menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-1456"><a href="http://www.siddillon.com/contact-us/">Locations</a>
<ul class="sub-menu">
	<li id="menu-item-1723" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1723"><a href="http://www.siddillon.com/chevrolet-blair/">Chevrolet Blair</a></li>
	<li id="menu-item-1724" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1724"><a href="http://www.siddillon.com/ford-ceresco/">Ford Ceresco</a></li>
	<li id="menu-item-1719" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1719"><a href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/">Chevrolet Buick Dodge Chrysler Ram Jeep Ford Lincoln Crete</a></li>
	<li id="menu-item-1718" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1718"><a href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/">Buick GMC Cadillac Mazda Fremont</a></li>
	<li id="menu-item-1722" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1722"><a href="http://www.siddillon.com/chevrolet-fremont/">Chevrolet Fremont</a></li>
	<li id="menu-item-1720" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1720"><a href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/">Buick Nissan Hyundai Lincoln</a></li>
	<li id="menu-item-1721" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1721"><a href="http://www.siddillon.com/chevrolet-buick-wahoo/">Chevrolet Buick Wahoo</a></li>
	<li id="menu-item-2576" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2576"><a href="http://www.siddillon.com/sid-dillon-o-street-lincoln/">Sid Dillon “O” Street Lincoln</a></li>
	<li id="menu-item-1996" class="menu-item menu-item-type-custom menu-item-object-custom top-level-empty menu-item-1996"><a target="_blank" href="//www.dillon-brothers.com/">Dillon Brothers Motorcycles</a></li>
</ul>
</li>
<li id="menu-item-64" class="right-edge menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-64"><a href="http://www.siddillon.com/about-us/">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-65"><a href="http://www.siddillon.com/about-us/">Our Dealerships</a></li>
	<li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-66"><a href="http://www.siddillon.com/contact-us/">Contact Us</a></li>
	<li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-67"><a href="http://www.siddillon.com/staff/">Staff</a></li>
	<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-68"><a href="http://www.siddillon.com/careers/">Careers</a></li>
	<li id="menu-item-2305" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2305"><a href="http://www.siddillon.com/videos/">Videos</a></li>
	<li id="menu-item-1970" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1970"><a href="http://www.siddillon.com/blog/">Blog</a></li>
</ul>
</li>
<li id="menu-item-3201" class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-3201"><a href="http://www.siddillon.com/research-page/">Research</a></li>
</ul>						</div>
					</div>
				</div>
			</div>	
			<div class="header-info">
								<span class="why-buy">Why buy from us? Learn about <a class="button primary-button small experience-button">THE SID DILLON EXPERIENCE</a></span>
			</div>
		</div>		
</div>		
	
<div id="experience-pulldown" class="hidden-sm hidden-xs">
	<div class="container-wide">
		<h2>The Sid Dillon Experience</h2>
		
		<div class="row">
			<div class="col-sm-4">
				<img src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/icon-no-doc-fees.png" alt="No Doc Fees">
				<h3>No documentation fees</h3>
			</div>
			<div class="col-sm-4">
				<img src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/icon-family-owned.png" alt="Family Owned">
				<h3>Family owned &amp; operated since 1976</h3>
			</div>
			<div class="col-sm-4">
				<img src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/icon-courtesy-transportation.png" alt="Courtesy Transportation">
				<h3>Courtesy transportation</h3>
			</div>
		</div>
	</div>
	<div class="experience-close-button"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
</div>	
	
		
<div class="visible-sm visible-xs">
	
<div id="tablet-basic-header" class="menu-top" style="left: 0px;">
  <a class="mobile-menu-toggle menu-hamburger" id="desktop-menu-toggle" data-side="left" data-container="sidr-main-menu" style="cursor:pointer;">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-label">MENU</span>
  </a>
  <span class="logo" itemscope="" itemtype="http://schema.org/Organization">
    <a itemprop="url" href="http://www.siddillon.com" data-gtm-event="tabletHeaderLogo">
      <img itemprop="logo" src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/logo-head-mobile.png" alt="Sid Dillon Auto Group">
    </a>
  </span>

  
	<div class="text-right">
	  <a id="desktop-locations-toggle" data-side="right" data-container="sidr-desktop-locations" class="locations-toggle menu"><span class="menu-label">LOCATIONS</span><span class="glyphicon glyphicon-map-marker"></span><span class="glyphicon glyphicon-remove"></span></a>
  </div>

  
  </div>

<div class="fixed-top-spacer"></div>


<script>
$(document).ready(function() {
  $('#desktop-locations-toggle').sidr({
	  name: 'sidr-desktop-locations',
	  side: 'right'
  });
  
  $('#desktop-menu-toggle').sidr({
	  name: 'sidr-main-menu',
	  side: 'left'
  });
  
  $(".experience-button").click(function() {
  	$("#experience-pulldown").toggleClass('open')
  })
  
});
</script>

<div id="sidr-desktop-locations" class="mobile-nav-menu sidebar-navigation-enabled sidr right" style="display: none;">
	<ul class="dealerlist"><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Chevrolet Blair</span><span class="dealeraddress">2261 S Hwy 30 <br>Blair, NE 68008</span><span class="dealerphone">(402) 426-4121</span><a href="/chevrolet-blair/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Ford Ceresco</span><span class="dealeraddress">305 2nd Street <br>Ceresco, NE 68017</span><span class="dealerphone">(877) 834-9243</span><a href="/ford-ceresco/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Chevrolet Buick Dodge Chrysler Ram Jeep Ford Lincoln Crete</span><span class="dealeraddress">2455 E. Hwy 33 <br>Crete, NE 68333</span><span class="dealerphone">(402) 826-2668</span><a href="/sid-dillon-chevrolet-buick-cdjr-crete/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Buick GMC Cadillac Mazda Fremont</span><span class="dealeraddress">2420 E 23rd Street <br>Fremont, NE 68025</span><span class="dealerphone">(888) 476-1090</span><a href="/buick-gmc-cadillac-mazda-fremont/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Chevrolet Fremont</span><span class="dealeraddress">2500 E 23rd Street <br>Fremont, NE 68025</span><span class="dealerphone">(877) 282-6883</span><a href="/chevrolet-fremont/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Nissan Hyundai Buick Lincoln</span><span class="dealeraddress">2627 Kendra Lane <br>Lincoln, NE 68512</span><span class="dealerphone">(402) 464-6500</span><a href="/buick-nissan-hyundai-lincoln/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><div class="dealer-list-content"><span class="dealername">Sid Dillon Chevrolet Buick Wahoo</span><span class="dealeraddress">257 West A Street <br>Wahoo, NE 68066</span><span class="dealerphone">(800) 677-1180</span><a href="/chevrolet-buick-wahoo/" class="button primary-button small">Visit Location</a></div></div></li><li class="menulist"><div class="dealer-listing"><a class="linkwrap" target="_blank" href="//www.dillon-brothers.com"><div class="dealer-thumb"><img src="/wp-content/themes/DealerInspireDealerTheme/images/dillonbros-thumb.jpg"><div class="thumb-overlay">Visit Location</div></div></a><div class="dealer-list-content motorcycle-item"><a class="linkwrap" target="_blank" href="//www.dillon-brothers.com"><span class="dealername">DILLON BROTHERS MOTORCYCLE DEALERSHIPS</span><span class="dealerphone">OMAHA<br>(402) 289-5556</span><span class="dealerphone">FREMONT<br>(402) 721-2007</span></a><a target="_blank" class="button primary-button block" href="//www.dillon-brothers.com/">Visit Location</a></div></div></li></ul><div class="close-mobile-wrap-side"><span class="close-mobile-nav"><i class="fa fa-close"></i> close</span></div>
</div>
</div>		

<div class="visible-sm visible-xs">
	
<div id="sidr-main-menu" class="mobile-nav-menu sidebar-navigation-enabled sidr left" style="display: none;">
	<div class="menu-row">
		<div class="menu-span">
			<div class="navbar">
				<div class="navbar-inner">
										<div class="nav_section">
						<ul id="menu-main-menu-1" class="nav"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3 current_page_item top-level-empty menu-item-53 active"><a title="Home" href="http://www.siddillon.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-54 dropdown"><a title="New Vehicles" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">New Vehicles <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-55"><a title="View All New Vehicles" href="http://www.siddillon.com/new-vehicles/">View All New Vehicles</a></li>
	<li class="make-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2095 dropdown"><a title="View Vehicles By Make" href="http://www.siddillon.com/vehicle-makes/">View Vehicles By Make</a>
	<ul role="menu" class=" dropdown-menu">
		<li class="menu-half di-vehicle-icon buick-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1388"><a title="Buick" href="http://www.siddillon.com/new-vehicles/buick/">Buick</a></li>
		<li class="menu-half di-vehicle-icon cadillac-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1389"><a title="Cadillac" href="http://www.siddillon.com/new-vehicles/cadillac/">Cadillac</a></li>
		<li class="menu-half di-vehicle-icon chevrolet-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1390"><a title="Chevrolet" href="http://www.siddillon.com/new-vehicles/chevrolet/">Chevrolet</a></li>
		<li class="menu-half di-vehicle-icon chrysler-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1391"><a title="Chrysler" href="http://www.siddillon.com/new-vehicles/chrysler/">Chrysler</a></li>
		<li class="menu-half di-vehicle-icon dodge-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1379"><a title="Dodge" href="http://www.siddillon.com/new-vehicles/dodge/">Dodge</a></li>
		<li class="menu-half di-vehicle-icon ford-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1380"><a title="Ford" href="http://www.siddillon.com/new-vehicles/ford/">Ford</a></li>
		<li class="menu-half di-vehicle-icon gmc-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1381"><a title="GMC" href="http://www.siddillon.com/new-vehicles/gmc/">GMC</a></li>
		<li class="menu-half di-vehicle-icon hyundai-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1382"><a title="Hyundai" href="http://www.siddillon.com/new-vehicles/hyundai/">Hyundai</a></li>
		<li class="menu-half di-vehicle-icon jeep-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1383"><a title="Jeep" href="http://www.siddillon.com/new-vehicles/jeep/">Jeep</a></li>
		<li class="menu-half di-vehicle-icon lincoln-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1384"><a title="Lincoln" href="http://www.siddillon.com/new-vehicles/lincoln/">Lincoln</a></li>
		<li class="menu-half di-vehicle-icon mazda-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1385"><a title="Mazda" href="http://www.siddillon.com/new-vehicles/mazda/">Mazda</a></li>
		<li class="menu-half di-vehicle-icon nissan-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1386"><a title="Nissan" href="http://www.siddillon.com/new-vehicles/nissan/">Nissan</a></li>
		<li class="menu-half di-vehicle-icon ram-logo hide-count menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1387"><a title="Ram" href="http://www.siddillon.com/new-vehicles/ram/">Ram</a></li>
	</ul>
</li>
	<li class="location-menu shift-up-2 menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2094 dropdown"><a title="View Vehicles By Location" href="http://www.siddillon.com/vehicle-locations/">View Vehicles By Location</a>
	<ul role="menu" class=" dropdown-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1398"><a title="Sid Dillon Chevrolet Blair" href="http://www.siddillon.com/chevrolet-blair/new-vehicles/">Sid Dillon Chevrolet Blair</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1397"><a title="Sid Dillon Ford Ceresco" href="http://www.siddillon.com/ford-ceresco/new-vehicles/">Sid Dillon Ford Ceresco</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1399"><a title="Sid Dillon Crete" href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/new-vehicles/">Sid Dillon Crete</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1394"><a title="Sid Dillon Buick GMC Cadillac Mazda Fremont" href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/new-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1393"><a title="Sid Dillon Chevy Fremont" href="http://www.siddillon.com/chevrolet-fremont/new-vehicles/">Sid Dillon Chevy Fremont</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1395"><a title="Sid Dillon Lincoln" href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/new-vehicles/">Sid Dillon Lincoln</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1396"><a title="Sid Dillon Chevrolet Buick Wahoo" href="http://www.siddillon.com/chevrolet-buick-wahoo/new-vehicles/">Sid Dillon Chevrolet Buick Wahoo</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-56 dropdown"><a title="Used Vehicles" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Used Vehicles <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-60"><a title="View All Used Cars" href="http://www.siddillon.com/used-vehicles/">View All Used Cars</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-1404 dropdown"><a title="Used Vehicles By Location" href="http://www.siddillon.com/used-vehicles/">Used Vehicles By Location</a>
	<ul role="menu" class=" dropdown-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1405"><a title="Sid Dillon Chevrolet Blair" href="http://www.siddillon.com/chevrolet-blair/used-vehicles/">Sid Dillon Chevrolet Blair</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1406"><a title="Sid Dillon Ford Ceresco" href="http://www.siddillon.com/ford-ceresco/used-vehicles/">Sid Dillon Ford Ceresco</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1408"><a title="Sid Dillon Crete" href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/used-vehicles/">Sid Dillon Crete</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1409"><a title="Sid Dillon Buick GMC Cadillac Mazda Fremont" href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/used-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1407"><a title="Sid Dillon Chevrolet Fremont" href="http://www.siddillon.com/chevrolet-fremont/used-vehicles/">Sid Dillon Chevrolet Fremont</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1410"><a title="Sid Dillon Buick Nissan Hyundai Lincoln" href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/used-vehicles/">Sid Dillon Buick Nissan Hyundai Lincoln</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1411"><a title="Sid Dillon Chevrolet Buick Wahoo" href="http://www.siddillon.com/chevrolet-buick-wahoo/used-vehicles/">Sid Dillon Chevrolet Buick Wahoo</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-57 dropdown"><a title="Certified Pre-Owned Vehicles" href="http://www.siddillon.com/used-vehicles/certified-pre-owned-vehicles/">Certified Pre-Owned Vehicles</a>
	<ul role="menu" class=" dropdown-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1653"><a title="Sid Dillon Chevrolet Blair Certified" href="http://www.siddillon.com/chevrolet-blair/certified-pre-owned-vehicles/">Sid Dillon Chevrolet Blair Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1652"><a title="Sid Dillon Ford Ceresco Certified" href="http://www.siddillon.com/ford-ceresco/certified-pre-owned-vehicles/">Sid Dillon Ford Ceresco Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1651"><a title="Sid Dillon Crete Certified" href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/certified-pre-owned-vehicles/">Sid Dillon Crete Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1650"><a title="Sid Dillon Buick GMC Cadillac Mazda Fremont Certified" href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/certified-pre-owned-vehicles/">Sid Dillon Buick GMC Cadillac Mazda Fremont Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1649"><a title="Sid Dillon Chevrolete Fremont Certified" href="http://www.siddillon.com/chevrolet-fremont/certified-pre-owned-vehicles/">Sid Dillon Chevrolete Fremont Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1648"><a title="Sid Dillon Buick Nissan Hyundai Lincoln Certified" href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/certified-pre-owned-vehicles/">Sid Dillon Buick Nissan Hyundai Lincoln Certified</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1647"><a title="Sid Dillon Chevrolet Buick Wahoo Certified" href="http://www.siddillon.com/chevrolet-buick-wahoo/certified-pre-owned-vehicles/">Sid Dillon Chevrolet Buick Wahoo Certified</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-58"><a title="Priced Under 10k" href="http://www.siddillon.com/used-vehicles/priced-under-10k/">Priced Under 10k</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1402"><a title="Value Your Trade" href="http://www.siddillon.com/sid-dillon-value-your-trade/">Value Your Trade</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-2322 dropdown"><a title="Specials" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Specials <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-4726"><a title="Video Library" href="http://www.siddillon.com/video-library/">Video Library</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-61 dropdown"><a title="Finance Centers" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Finance Centers <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1413"><a title="Finance Centers" href="http://www.siddillon.com/finance/">Finance Centers</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1412"><a title="Apply For Financing" href="http://www.siddillon.com/finance/">Apply For Financing</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-63"><a title="Payment Calculator" href="http://www.siddillon.com/payment-calculator/">Payment Calculator</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1403"><a title="Value Your Trade" href="http://www.siddillon.com/sid-dillon-value-your-trade/">Value Your Trade</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-69 dropdown"><a title="Service &amp; Parts" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Service &amp; Parts <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1433"><a title="Service Centers" href="http://www.siddillon.com/service/">Service Centers</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2297"><a title="Schedule Service" href="http://www.siddillon.com/service/">Schedule Service</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1434"><a title="Body Shops" href="http://www.siddillon.com/body-shops/">Body Shops</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-71"><a title="Parts Departments" href="http://www.siddillon.com/parts/">Parts Departments</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-72"><a title="Order Your Vehicle Parts" href="http://www.siddillon.com/parts/order-parts/">Order Your Vehicle Parts</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1441"><a title="Accessories" href="http://www.siddillon.com/accessories/">Accessories</a></li>
</ul>
</li>
<li class="right-edge menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-1456 dropdown"><a title="Locations" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Locations <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1723"><a title="Chevrolet Blair" href="http://www.siddillon.com/chevrolet-blair/">Chevrolet Blair</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1724"><a title="Ford Ceresco" href="http://www.siddillon.com/ford-ceresco/">Ford Ceresco</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1719"><a title="Chevrolet Buick Dodge Chrysler Ram Jeep Ford Lincoln Crete" href="http://www.siddillon.com/sid-dillon-chevrolet-buick-cdjr-crete/">Chevrolet Buick Dodge Chrysler Ram Jeep Ford Lincoln Crete</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1718"><a title="Buick GMC Cadillac Mazda Fremont" href="http://www.siddillon.com/buick-gmc-cadillac-mazda-fremont/">Buick GMC Cadillac Mazda Fremont</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1722"><a title="Chevrolet Fremont" href="http://www.siddillon.com/chevrolet-fremont/">Chevrolet Fremont</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1720"><a title="Buick Nissan Hyundai Lincoln" href="http://www.siddillon.com/buick-nissan-hyundai-lincoln/">Buick Nissan Hyundai Lincoln</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1721"><a title="Chevrolet Buick Wahoo" href="http://www.siddillon.com/chevrolet-buick-wahoo/">Chevrolet Buick Wahoo</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2576"><a title="Sid Dillon &quot;O&quot; Street Lincoln" href="http://www.siddillon.com/sid-dillon-o-street-lincoln/">Sid Dillon “O” Street Lincoln</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom top-level-empty menu-item-1996"><a title="Dillon Brothers Motorcycles" target="_blank" href="//www.dillon-brothers.com/">Dillon Brothers Motorcycles</a></li>
</ul>
</li>
<li class="right-edge menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children top-level-children menu-item-64 dropdown"><a title="About Us" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">About Us <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-65"><a title="Our Dealerships" href="http://www.siddillon.com/about-us/">Our Dealerships</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-66"><a title="Contact Us" href="http://www.siddillon.com/contact-us/">Contact Us</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-67"><a title="Staff" href="http://www.siddillon.com/staff/">Staff</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-68"><a title="Careers" href="http://www.siddillon.com/careers/">Careers</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-2305"><a title="Videos" href="http://www.siddillon.com/videos/">Videos</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-1970"><a title="Blog" href="http://www.siddillon.com/blog/">Blog</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page top-level-empty menu-item-3201"><a title="Research" href="http://www.siddillon.com/research-page/">Research</a></li>
</ul>					</div>
					<div class="close-mobile-wrap"><span class="close-mobile-nav"><i class="fa fa-close"></i> close</span></div>				</div>
			</div>
		</div>
	</div>
</div></div>