<div class="whiteBackgroundContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="QuestionLine">
					<div class="question">
						Does joining the club cost me anything?	
					</div>
					No! The Sid Dillon Referral Club is 100% free.  Actually we pay you!					
				</div>
				<div class="QuestionLine">
					<div class="question">
						Who can I refer?	
					</div>
					Referrals must be new to Sid Dillon or have not purchased a vehicle from Sid Dillin in the last 8 years.				
				</div>
				<div class="QuestionLine">
					<div class="question">
						Does my referral tier level reset?	
					</div>
					Yes, your referral tier level resets January 1st every year.				
				</div>
				<div class="QuestionLine">
					<div class="question">
						I forgot my login.  What do I do?	
					</div>
					Your login should be your email address.  You also should recieve an email with that information.
				</div>
				<div class="QuestionLine">
					<div class="question">
						What if I forgot my password?	
					</div>
					You can reset your password at this link.				
				</div>
				<div class="QuestionLine">
					<div class="question">
						Can multiple people use one referral account?	
					</div>
					Yes, but only immediate familiy members living in the same household.				
				</div>
				<div class="QuestionLine">
					<div class="question">
						Do I get paid after I submit a refferal?
					</div>
					No, you only get paid once your referral purchases a vehicle	
				</div>
				<div class="QuestionLine">
					<div class="question">
						What is a referral number?
					</div>
					Your unique referral id used to identify and pay you.	
				</div>
				<div class="QuestionLine">
					<div class="question">
						Will my referral be added to the price of my referrals vehicle purchase?
					</div>
					No, we never add referral costs to any vehicle sales.
				</div>
				<div class="QuestionLine">
					<div class="question">
						Can I check my referral status?
					</div>
					Yes, just login to your account here and we show your active and paid referrals.
				</div>	
				<div class="QuestionLine">				
					<div class="question">
						I moved can I update my address?
					</div>
					Yes login to your account and update any contact information there.
				</div>
				
			</div>
		</div>

		
	</div>
			
</div>

