<div class="whiteBackgroundContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Your Profile
				</div>
			</div>
		</div>
		<div class="row" id="validationSummaryMessageElement">
			<div class="col-md-12">
				<div class="alert alert-danger" id="messageTypeColor">
					<div id="divErrorHeader" style="font-weight:bold;"></div>
					<span id="validationSummaryMessageText"></span>					
				</div>
			
				
			</div>
		</div>
		
		<form id="ProfileUpdate" method="post" action="<?php echo PATH ?>member/save/profile">
			<div class="row rowSpacing iconSeparator">
				<div class="col-md-12">
					<img src="<?php echo PATH ?>public/images/ContactInfo.png">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">Your Club ID</div>	
						<div class="clubIDText">SD<?php echo $this -> profileInfo -> ClubID?></div>
					</div>
					
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID2">
						<div class="inputLabel">First Name<span class="errorMessage"></span></div>	
						<input type="text" name="firstName" value="<?php echo $this -> profileInfo -> firstName?>" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID3">
						<div class="inputLabel">Last Name<span class="errorMessage"></span></div>	
						<input type="text" name="lastName" value="<?php echo $this -> profileInfo -> lastName?>" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID4">
						<div class="inputLabel">Email<span class="errorMessage"></span></div>	
						<input type="text" name="email" value="<?php echo $this -> profileInfo -> email?>" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID5">
						<div class="inputLabel">Phone<span class="errorMessage"></span></div>	
						<input type="text" name="phone" id="PhoneInput" value="<?php echo $this -> profileInfo -> Phone?>" />
					</div>
				</div>
			</div>	
			<div class="row rowSpacing iconSeparator" style="margin-top:50px;">
				<div class="col-md-12">
					<img src="<?php echo PATH ?>public/images/Address.png">
				</div>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" id="inputID6">
						<div class="inputLabel">Address<span class="errorMessage"></span></div>	
						<input type="text" name="address" value="<?php echo $this -> profileInfo -> address?>" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="inputLine" id="inputID7">
						<div class="inputLabel">City<span class="errorMessage"></span></div>	
						<input type="text" name="city" value="<?php echo $this -> profileInfo -> city?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="inputLine" id="inputID8">
						<div class="inputLabel">State<span class="errorMessage"></span></div>	
						<select name="addressState" class="stateDropDown" onchange="RegisterController.ChangeStateValue(this, 8)">
							<option value="0"></option>
							<?php foreach($this -> states as $key => $value) : ?>
								<option <?php echo $this -> profileInfo -> State == $key ? 'selected' : ''; ?> value="<?php echo $key ?>"><?php echo $key ?></option>
							<?php endforeach; ?>								
						</select>
						<input type="hidden" name="hiddenState" id="hiddenState" value="<?php echo $this -> profileInfo -> State?>" />
						<!--<input type="text" name="state" maxlength="2" onkeyup="RegisterController.removeValidationMessage(8)" /> -->
					</div>
				</div>
				<div class="col-md-2">
					<div class="inputLine" id="inputID9">
						<div class="inputLabel">Zip<span class="errorMessage"></span></div>	
						<input type="text" name="Zip" value="<?php echo $this -> profileInfo -> zip?>" />
					</div>
				</div>
			</div>
			
			<div class="row rowSpacing" style="margin-top:70px;">
				<div class="col-md-12 sectionHeader">
					Your Credentials
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID10">
						<div class="inputLabel">Username<span class="errorMessage"></span></div>	
						<input type="text" name="userName" value="<?php echo $this -> profileInfo -> userName?>" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID11">
						<div class="inputLabel">Password<span class="errorMessage"></span></div>
						<input type="password" name="password" onkeyup="Globals.removeValidationMessage(11)">
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<input type="submit" value="SAVE" class="redButton submitButton">
					</div>
				</div>
			</div>
		</form>

		
	</div>
	
	
</div>


