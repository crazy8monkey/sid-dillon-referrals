<div class="whiteBackgroundContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Referral Program Rules
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="javascript:void(0)" onclick="MemberController.toggleReferralTierInfo();">
					<div class="referralTierPlus" style="float:left; font-size:30px; font-weight:bold; color: #1e5fa8; width:20px; text-align:center;">+</div>
					<div style="font-size: 24px; float: left; margin-top: 5px; margin-left: 9px; color:black">Referral Tier</div>	
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" id="TierList">
				<div style="margin-top:10px;">
					<table class="tierTable" style="margin-top: 10px;">
						<?php foreach($this -> referralTiers as $tier) : ?>
							<tr>
								<td>
									<div class="content tierText" style="float:left; padding-right:5px;">
										<?php echo $tier['Level'] ?>
									</div>
									<div class="divider"></div>
								</td>
								<td class="price">
									<div class="content price" style="float:right; padding-left:5px;">
										$<?php echo $tier['Amount'] ?>
									</div>
									<div class="divider"></div>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>					
				</div>

			</div>
		</div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-12">
				<a href="javascript:void(0)" onclick="MemberController.toggleRulesRegulationsInfo();">
					<div class="referralRulesText" style="float:left; font-size:30px; font-weight:bold; color: #1e5fa8; width:20px; text-align:center;">+</div>
					<div style="font-size: 24px; float: left; margin-top: 5px; margin-left: 9px; color:black">Rules / Regulations</div>	
				</a>
				
			</div>
		</div>
		<div class="row" id="RulesRegulationsText">
			<div class="col-md-12">
				<?php include 'view/shared/rules-regulations.php' ?>
			</div>
		</div>
		
	</div>
	
	
</div>


