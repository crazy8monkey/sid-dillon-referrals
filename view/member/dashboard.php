<?php $startOfYear = date("M j, Y", strtotime('first day of January '.date('Y'))) ?>
<?php $endOfYear = date("M j, Y", strtotime('last day of December '.date('Y'))) ?>
<div class="whiteBackgroundContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Hello <?php echo $this -> MemberInfo -> firstName ?> <?php echo $this -> MemberInfo -> lastName ?>!
				</div>
				
			</div>
		</div>
		<div class="row" style="margin-top:10px;">
			<div class="col-md-12">
				<strong>Your Club ID: </strong>SD<?php echo $this -> MemberInfo -> ClubID ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 SectionHeader" style="margin-bottom:20px">
				<div class="icon">
					<img src='<?php echo PATH ?>public/images/EarningsIcon.png' />
				</div>
				
					Your Earnings<?php echo $this -> tooltip("List of referrals thats been sold within this current year<br /> (" .$startOfYear . ' - '. $endOfYear . ")") ?>	
				
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 EarningsList">
				<div class="ListContent">
					<?php if(!empty($this -> EarnedReferrals)) : ?>
						<?php $grandTotal = ""; ?>
						<?php foreach ($this -> EarnedReferrals as $referral) : ?>
							<div class="EarningLine" style="clear:both">
								<div class="referralName">
									<a style="text-decoration:underline;" href="javascript:void(0)" onclick="MemberController.getReferralSingle(<?php echo $referral['referralID'] ?>, 'sold')"><?php echo $referral['FirstName'] ?> <?php echo $referral['LastName'] ?></a>		
								</div>
								<div class="Amount" style="float:right">
									$<?php echo $referral['PaidAmount'] ?>
								</div>
								<div class="divider"></div>
								<div style="clear:both"></div>
							</div>
							<?php $grandTotal += $referral['PaidAmount']; ?>
						<?php endforeach; ?>
						<div class="GrandTotalLine">
						
						<div class="grandTotalNumber">
							$<?php echo $grandTotal ?>	
						</div>
						<div class="label">
							Grand Total:
						</div>
						<div style="clear:both"></div>
					</div>
						
						
					<?php else: ?>
						You currently have no referrals that have purchases vehicles, submit referrals by clicking <a style="text-decoration:underline; font-size:16px;" href="<?php echo PATH ?>submit">here.</a>
					<?php endif; ?>					
				</div>

				
			</div>
		</div>
		<div class="row">
			<div class="dashboardDivider"></div>
			<div class="col-md-12 SectionHeader">
				<div class="icon">
					<img src='<?php echo PATH ?>public/images/Active.png' />
				</div>
				Active Referrals<?php echo $this -> tooltip("List of referrals that are in review within this current year<br /> (" .$startOfYear . ' - '. $endOfYear . ")") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 ActiveList">
				<div class="ListContent">
					<?php if(empty($this -> ActiveReferrals)) : ?>
						You currently have no active referrals at this moment, submit referrals by clicking <a style="text-decoration:underline; font-size:16px;" href="<?php echo PATH ?>submit">here.</a> 
					<?php endif; ?>
					<?php if(!empty($this -> ActiveReferrals)) : ?>
						<?php foreach ($this -> ActiveReferrals as $activeReferral) : ?>
							<span class="bullet">&bull;</span><a style="text-decoration:underline;" href="javascript:void(0)" onclick="MemberController.getReferralSingle(<?php echo $activeReferral['referralID'] ?>, 'active')"><?php echo $activeReferral['FirstName'] ?> <?php echo $activeReferral['LastName'] ?></a>
						<?php endforeach; ?> 
					<?php endif; ?>
				</div>
				
				
			</div>
		</div>
		<div class="row">
			<div class="dashboardDivider"></div>
			<div class="col-md-12 SectionHeader">
				<div class="icon">
					<img src='<?php echo PATH ?>public/images/InActive.png' />
				</div>
				Inactive Referrals<?php echo $this -> tooltip("List of referrals that are no longer in review within this current year<br /> (" .$startOfYear . ' - '. $endOfYear . ")") ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 InactiveList">
				<div class="ListContent">
					<?php if(empty($this -> InactiveReferrals)) : ?>
						You currently have no inactive referrals at this moment, submit referrals by clicking <a style="text-decoration:underline; font-size:16px;" href="<?php echo PATH ?>submit">here.</a> 
					<?php endif; ?>
					<?php if(!empty($this -> InactiveReferrals)) : ?>
						<?php foreach ($this -> InactiveReferrals as $inactiveReferral) : ?>
							<span class="bullet">&bull;</span><a style="text-decoration:underline;" href="javascript:void(0)" onclick="MemberController.getReferralSingle(<?php echo $inactiveReferral['referralID'] ?>, 'inactive')"><?php echo $inactiveReferral['FirstName'] ?> <?php echo $inactiveReferral['LastName'] ?></a>
						<?php endforeach; ?> 
					<?php endif; ?>
				</div>				
			</div>
		</div>
	</div>
	
	
</div>
<div class="WhitePopupForm" id="ReferalDetail">
	<div class="header">
		Referral Detail
		<a href="javascript:void(0)" onclick="Globals.ClosePopups()">
			<div class="close" style="margin-top: 18px;">
				<i class="fa fa-times-circle" aria-hidden="true"></i>
			</div>			
		</a>
	</div>
	<div class="Content">
		<div class="line">
			<div class="contentLabel">
				Submitted
			</div>
			<div id="submittedDate"></div>
		</div>
		<div class="line">
			<div class="contentLabel">
				Store
			</div>
			<div id="storeName"></div>
		</div>		
		<div class="line">
			<div class="contentLabel">
				Salesperson
			</div>
			<div id="SalesPerson"></div>
		</div>
		<div class="line" id="soldLine">
			<div class="contentLabel">
				Sold
			</div>
			<div id="SoldDateContent"></div>
		</div>
		<div class="line" id="ReferralMemberNotes">
			<div class="contentLabel">
				Referral notes
			</div>
			<div id="ReferralMemberNotesContent"></div>
		</div>
		
	</div>
</div>


