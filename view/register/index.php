<div class="whiteBackgroundContent">
	<div class="container" id="RegisterToClubForm">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Register to our club
				</div>
			</div>
		</div>
		<div class="row" style="padding-bottom:20px; margin-bottom:-10px">
			<div class="col-md-6">
				Registering for the Sid Dillon Referral club is easy, just fill in the required information below.
Once complete, you will gain access to submit referrals that have the potential of earning you money!
If you have any questions feel free to read our <a href="<?php echo PATH ?>faq">FAQ</a>  or contact us at <a href="mailto:referral@siddillon.com">referral@siddillon.com</a> 
				
			</div>
			<div class="col-md-6">
				*Sid Dillon reserves the right to modify the terms and conditions of this program and may cancel this program at any time without notification to its members.
			</div>
		</div>
		<form id="newRegisterUser" method="post" action="<?php echo PATH ?>register/save/register">
			<div class="row">
				<div class="col-md-12">
					<div id="validationSummaryMessageElement" class="alert alert-danger">
						<div id="divErrorHeader" style='font-weight:bold; margin-bottom:10px;'></div>
						<span id="validationSummaryMessageText"></span>	
					</div>
				</div>
			</div>
			
			<div class="row rowSpacing" style="margin-bottom: 10px;">
				<div class="col-md-12 sectionHeader">
					Your Contact Information
				</div>
			</div>
			<div class="SidDillonRelationContainer">
				<div class="row" id="inputID13">
					<div class="col-md-12">
						Are you a Sid Dillon Employee, or an immediate family member to a Sid Dillon employee?<span class="errorMessage"></span>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<div style="margin:10px 0px;">
							<input type="radio" value="1" name="SidDillonRelation" onchange="Globals.removeValidationMessage(13)" /><label for="SidDillonRelation">Yes</label>	
							<div style="clear:both"></div>
						</div>
						<div>
							<input type="radio" value="0" name="SidDillonRelation" onchange="Globals.removeValidationMessage(13)" /><label for="SidDillonRelation">No</label>
							<div style="clear:both"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row rowSpacing iconSeparator">
				<div class="col-md-12">
					<img src="<?php echo PATH ?>public/images/ContactInfo.png" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID2">
						<div class="inputLabel">First Name *<span class="errorMessage"></span></div>	
						<input type="text" name="firstName" onkeyup="Globals.removeValidationMessage(2)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID3">
						<div class="inputLabel">Last Name *<span class="errorMessage"></span></div>
						<input type="text" name="lastName" onkeyup="Globals.removeValidationMessage(3)" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID4">
						<div class="inputLabel">Email *<span class="errorMessage"></span></div>	
						<input type="text" name="email" onkeyup="Globals.removeValidationMessage(4)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID5">
						<div class="inputLabel">Phone *<span class="errorMessage"></span></div>
						<input type="text" name="phone" id="phoneInput" onkeyup="Globals.removeValidationMessage(5)" />
					</div>
				</div>
			</div>
			<div class="row rowSpacing iconSeparator" style='margin-top:50px;'>
				<div class="col-md-12">
					<img src="<?php echo PATH ?>public/images/Address.png" />
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" id="inputID6">
						<div class="inputLabel">Address *<span class="errorMessage"></span></div>	
						<input type="text" name="address" onkeyup="Globals.removeValidationMessage(6)" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="inputLine" id="inputID7">
						<div class="inputLabel">City *<span class="errorMessage"></span></div>	
						<input type="text" name="city" onkeyup="Globals.removeValidationMessage(7)" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="inputLine" id="inputID8">
						<div class="inputLabel">State *<span class="errorMessage"></span></div>	
						<select name="addressState" class="stateDropDown" onchange="RegisterController.ChangeStateValue(this, 8)">
							<option value="0"></option>
							<?php foreach($this -> states as $key => $value) : ?>
								<option value="<?php echo $key ?>"><?php echo $key ?></option>
							<?php endforeach; ?>
						</select>
						<input type="hidden" name="hiddenState" id="hiddenState" />
						<!--<input type="text" name="state" maxlength="2" onkeyup="RegisterController.removeValidationMessage(8)" /> -->
					</div>
				</div>
				<div class="col-md-2">
					<div class="inputLine" id="inputID9">
						<div class="inputLabel">Zip *<span class="errorMessage"></span></div>	
						<input type="text" name="Zip" onkeyup="Globals.removeValidationMessage(9)" />
					</div>
				</div>
			</div>
			<div class="row rowSpacing" style="margin-top:70px;">
				<div class="col-md-12 sectionHeader">
					Your Credentials
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine" id="inputID10">
						<div class="inputLabel">Username *<span class="errorMessage"></span></div>	
						<input type="text" name="userName" onkeyup="Globals.removeValidationMessage(10)" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine" id="inputID11">
						<div class="inputLabel">Password *<span class="errorMessage"></span></div>
						<input type="password" name="password" onkeyup="Globals.removeValidationMessage(11)" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<div class="inputLine" id="inputID12">
						<input type="checkbox" name="agreement" onchange="RegisterController.showTermsConditions(this, 12)" /> I have read and agree to the Rules and Regulations
						<div style="margin-top:5px; margin-left: 0px;" class="errorMessage"></div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<input type="submit" value="REGISTER" class="redButton submitButton"  />
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
	<div class="container" id="ThankYouClubContent">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Thank you for signing up
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				Thank you for signing up for the Sid Dillon Referral Club!<br /><br />
				Your just a few clicks away from potentially making a lot of money!<br /><br />
				Sign in below to start referring!
			</div>
		</div>
		<div class="row" style="padding-bottom: 40px;">
			<div class="col-md-12" style="margin-top:10px;">
				<a href="<?php echo PATH ?>login">
					<div class="redButton submitButton" style="float:left;">
						LOGIN IN
					</div>					
				</a>

			</div>
		</div>
	</div>
	
	
</div>
<div class="WhitePopupForm" id="TermsAndConditions">
	<div class="header">
		Rules and Regulations
	</div>
	<?php include 'view/shared/rules-regulations.php' ?>
	<div class="ConditionButtonHolder">
		<a href="javascript:void(0)" class="agreeBtnLink" style="text-decoration:none;" >
			<div class="agreeBtn disabled">I AGREE</div>	
		</a>
		
		<div style="clear:both"></div>
	</div>
	
</div>
<div class="LoadingRegistration">
	<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
</div>

