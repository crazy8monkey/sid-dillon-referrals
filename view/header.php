<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
	<head>
		<title><?php echo isset($this->title) ? $this->title : 'Sid Dillon Referral'; ?></title>
		
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo PATH ?>public/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo PATH ?>public/images/favicon.png" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		
		
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		
		
		
		<?php if(isset($this->css))  {
			foreach ($this -> css as $css)  {	?>
			<link rel="stylesheet" href="<?php echo PATH  ?>public/css/<?php echo $css; ?>" /> 
		<?php }
		}
		?>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
		<script src="<?php echo PATH ?>public/js/maskedInput.js" type="text/javascript" type="text/javascript"></script>
		<script src="<?php echo PATH ?>public/js/Globals.js" type="text/javascript" type="text/javascript"></script>
		
		<?php if(isset($this->js))  {
			foreach ($this -> js as $js) {	?>
				<script type="text/javascript" src="<?php echo $js; ?>"></script>
			<?php }
		}
		?>
		
		<script type="text/javascript"> 
			document.createElement("header");
			document.createElement("footer");

			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip({html: true});
				<?php if(isset($this->startJsFunction)) : ?>
					<?php foreach ($this -> startJsFunction as $startJsFunction) : ?>	
						<?php echo $startJsFunction; ?>	
					<?php endforeach ?>
				<?php endif; ?>
			});
		</script>
		
		<?php if(isset($this -> ReferralSection)) : ?>
			<link rel='stylesheet' id='base-child-style-css'  href='//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/css/style.css?ver=1475605978' type='text/css' media='all' />
			<link rel='stylesheet' id='sidr-css-css'  href='//www.siddillon.com/wp-content/themes/DealerInspireCommonTheme/includes/css/jquery.sidr.dark.min.css?ver=4.4.1' type='text/css' media='all' />
			<script type='text/javascript' src='//www.siddillon.com/wp-content/themes/DealerInspireCommonTheme/includes/js/min/jquery.sidr.min.js?ver=4.4.1'></script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<?php endif; ?>
		
	</head>
<body>


<?php if(isset($this -> memberLoginSection)) : ?>
	<header id="MembershipLogin">
		<div class="container-fluid">
			<div class="col-xs-6" id="memberLoginText">
				<a href="<?php echo PATH ?>">
					<div class="btn" style="float:left">
						<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</div>	
				</a>
				<a class="referralMainText" style="text-decoration:none; color:white !important; margin-left: 10px; margin-top: 2px; float: left;" href="<?php echo PATH ?>">
					Referral Program
				</a>
			</div>
			<div class="col-xs-6">
				<div id="rightMenuContent">
					
					
					<div class="desktopView">
						
						<?php if(isset($this -> showMemberButtons)): ?>					
							<a href="<?php echo PATH ?>member/dashboard" data-toggle='tooltip' data-placement='bottom' title="Dashboard">
								<div class="btn headerBlueBtn" style=" padding: 2px 8px 3px 8px !important; font-size: 20px;">
									<i class="fa fa-home" aria-hidden="true"></i>
								</div>	
							</a>
							<a href="<?php echo PATH ?>member/profile" data-toggle='tooltip' data-placement='bottom' title="Profile">
								<div class="btn headerBlueBtn" style="padding: 6px 9px;">
									<img src='<?php echo PATH ?>public/images/memberProfile.png' />
								</div>	
							</a>
							<a href="<?php echo PATH ?>member/rules" data-toggle='tooltip' data-placement='bottom' title="Rules & Regulations">
								<div class="btn headerBlueBtn" style="padding: 6px 9px;">
									<img src='<?php echo PATH ?>public/images/Regulation.png' style="height: 20px;" />
								</div>	
							</a>
							<a href="<?php echo PATH ?>member/logout" data-toggle='tooltip' data-placement='bottom' title="Logout">
								<div class="btn headerBlueBtn" style="padding: 6px 9px;">
									<img src='<?php echo PATH ?>public/images/memberLogout.png' style="width: 20px;" />
								</div>	
							</a>
						<?php endif; ?>						
					</div>
					<?php if(isset($this -> showMemberButtons)): ?>
						<div class="mobileView">
							<a id="hrefMobileMenu" href="javascript:void(0)" onclick="Globals.ToggleMobileMenu()">
								<div style="color: white; font-size: 28px; height: 20px; margin-top: -3px;">
									<i class="fa fa-bars" aria-hidden="true"></i>	
								</div>							
							</a>
	
							<div class="menu">
								
									<a href="<?php echo PATH ?>member/dashboard">
										<div class="item blueMenuItem">
											<div class="icon">
												<i class="fa fa-home" aria-hidden="true"></i>
											</div>
											<div class="text">
												Dashboard	
											</div>
											<div style="clear:both"></div>
										</div>
									</a>
									<a href="<?php echo PATH ?>member/profile">
										<div class="item blueMenuItem">
											<div class="icon">
												<img src='<?php echo PATH ?>public/images/memberProfile.png' style="width: 13px;" />
											</div>
											<div class="text">
												Your Profile
											</div>
											<div style="clear:both"></div>
										</div>
									</a>
									<a href="<?php echo PATH ?>member/rules">
										<div class="item blueMenuItem">
											<div class="icon">
												<img src='<?php echo PATH ?>public/images/Regulation.png' style="width: 13px;" />
											</div>
											<div class="text">
												Rules / Regulations
											</div>
											<div style="clear:both"></div>
										</div>
									</a>
									<a href="<?php echo PATH ?>member/logout">
										<div class="item blueMenuItem">
											<div class="icon">
												<img src='<?php echo PATH ?>public/images/memberLogout.png' style="width: 13px;" />
											</div>
											<div class="text">
												Logout
											</div>
											<div style="clear:both"></div>
											
											
											
										</div>
									</a>
								
							</div>
						</div>
					<?php endif; ?>
					

				</div>	
			</div>
		</div>
	</header>
	<div style="height:75px"></div>
<?php endif; ?>	

<?php if(isset($this -> ReferralSection)) : ?>
	<header>
		<?php include 'view/shared/header-menu.php' ?>
	</header>
	<div class="push"></div>
	<div class="SidDillonReferralBackground">
		<div class="ReferralClubHeader">Sid Dillon Referral Club</div>
		<div class='subHeader'>Welcome to Sid Dillon Referral Club!</div>
	</div>
	<?php if(isset($this -> pageTitle)) : ?>
	
	<div class="storeSelectRow">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php echo $this -> pageTitle ?>
				</div>			
			</div>
	
		</div>
	</div>
	<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 MainMenu">
				<div class="MobileMenu">
					<a href="javascript:void(0)" id="hrefReferralMenu" onclick="Globals.ToggleReferralMenu();">
						<div style="float:left; color: #1e5fa8; font-size: 20px;">
							Referral Menu
						</div>
						<div style="float:right">
							<div style="color: #1e5fa8; font-size: 28px; height: 20px; margin-top: -6px;">
								<i class="fa fa-bars" aria-hidden="true"></i>	
							</div>
						</div>
						<div style="clear:both"></div>						
					</a>

				</div>
				
				<ul>
					<li><a href="<?php echo PATH ?>">Home</a></li>
					<li><a href="<?php echo PATH ?>faq">FAQ</a></li>
					<li><a href="<?php echo PATH ?>register">Register</a></li>
					<li><a href="<?php echo PATH ?>submit">Submit A Referral</a></li>
					<li><a href="<?php echo $this -> MemberLoginLink ?>"><?php echo $this -> MemberLoginLinkText?></a></li>
					<li><a href="<?php echo PATH ?>rules">Rules/Regulations</a></li>
					<li><a href="<?php echo PATH ?>contact">Contact</a></li>
				</ul>
			</div>			
		</div>
	</div>
<?php endif?>
