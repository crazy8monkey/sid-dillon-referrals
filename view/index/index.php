<div class="container">
	<div class="row ReferralClubIntro">
		<div class="col-md-6">
		Sid Dillon knows that our best form of advertising is you, our satisfied 
body of customers! Realizing this, we have developed the Sid Dillon 
Referral Program to thank you for sharing your positive experiences 
with others and recommending us to them.<br /><br />

		</div>
		<div class="col-md-6">
		Sid Dillon commits to treating your referrals with the same level of 
respect and dedication to satisfaction that we give you. The sales 
staff at Sid Dillon is excited about having you endorse us with your 
friends and family, and we are committed to making your referrals 
as fun and rewarding as you hope it will be.
		</div>		
	</div>	
</div>
<div class="whiteBackgroundContent">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="position:relative;">
				<div class="potentialMoney">$1,900+</div>
				<div class="tierDivider topDivider"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				Just think, by successfully referring a couple of friends, family, and co-workers per month, 
				you could receive $1,900+ or more per year as our way of saying, 
				”Thank You!” You won’t believe the possibilities! The best part of the Sid Dillon Referral Club 
				is getting a reward for successfully recommending us to your family, friends and co-workers.
			</div>
		</div>

		<div class="tierDivider" style='margin-top:30px;'></div>
		<div class="row" style="padding-bottom: 30px;">
			<div class="col-md-6">
				<div class="Indicator">
					<img src="<?php echo PATH?>public/images/credentials.png" />
				</div>
				Once the registration page is complete, Your Club ID (ex. SD98924)
				will be assgined which will be received via email.<br /><br />
				
				Your credentials will give you access to submit referrals and check the 
				status’s of all of your referrals you’ve entered.
			</div>
			<div class="col-md-6">
				<div class="Indicator">
					<img src="<?php echo PATH?>public/images/referral.png" />
				</div>
				Simply Login to your account and you will find all the information concerning 
				the club, submitting referrals at your fingertips. <br /><br />
				
				This information includes successful referral performance, reward level, 
				bonus rewards, and year-to-date successful referrals. We want 
				our Referral Partners to be informed on the recent specials.
			</div>
		</div>
		
	</div>
			
</div>

