<div class="Overlay" <?php echo isset($this -> HideClosePoup) ? '' : 'onclick="Globals.ClosePopups()"'; ?>></div>
<?php if(isset($this -> ReferralSection)) : ?>


<div class="container">
	<div class="row ReferralClubIntro" style="text-align:center;">
		<div class="col-md-12">
			<div class="buttonContainer">
				<a href="<?php echo PATH ?>register">
					<div class="blueButton">
						REGISTER	
					</div>
				</a>
				<a href="<?php echo PATH ?>submit">
					<div class="blueButton middleBlueButton">
						SUBMIT A REFERRAL	
					</div>
				</a>
				<a href="<?php echo $this -> FooterMemberLink ?>">
					<div class="blueButton">
						<?php echo $this -> FooterMemberText ?>
					</div>							
				</a>
			</div>
		</div>
		
	</div>	
</div>
<div id="footer">
	<div id="footer-top">
		<div class="container-wide">
			<div class="row">
				<div class="col-md-3 col-sm-4 footer-left">
					<a href="http://www.siddillon.com"><img src="//www.siddillon.com/wp-content/themes/DealerInspireDealerTheme/images/footer-left-logo.png" alt="Sid Dillon Auto Group"></a>
				</div>
					
				<div class="col-md-9 col-sm-8 footer-right">
					<div class="social">
								
		<a href="https://twitter.com/siddillon" target="_blank"><i class="fa fa-twitter"></i></a>						
		
		<a href="https://www.facebook.com/siddillonautogroup" target="_blank"><i class="fa fa-facebook"></i></a>						
		
		<a href="https://www.youtube.com/user/DillonCarsnTrucks" target="_blank"><i class="fa fa-youtube-play"></i></a>						
		
		<a href="https://plus.google.com/103516865755621308244/posts" target="_blank"><i class="fa fa-google-plus"></i></a>						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer-bottom">
		<div class="container-wide">
			<div class="row">
				
				<div class="col-md-12" style="text-align:center;">
					<span class="copyright">
						Copyright © <?php echo date("Y"); ?> Sid Dillon Auto Group					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
</body>	
</html>