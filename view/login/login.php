<div class="whiteBackgroundContent">
	<div class="container" id="FormContent">
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Membership Login
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 10px;">
				Due to a referral program overhaul we now require a user name instead of your referral club number. If you do not have a Username, <a href="<?php echo PATH ?>login/credentials/create" style="text-decoration:underline">Click here</a> to create one
			</div>
		</div>
		
		<form id="LoginForm" method="post" action="<?php echo PATH ?>login/save/userlogin">
			<?php echo $this -> form -> ValiationMessage("loginError", "loginLoading", "loginErrorMessage") ?>
			<div class="row rowSpacing">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">Username</div>
						<input type="text" name="username" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" id="inputID1">
						<div class="inputLabel">Password</div>
						<input type="password" name="password" />
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<div class="loginButton">
							<input type="submit" value="LOGIN" class="redButton submitButton" />	
						</div>
						<div class="forgotCredentialsLink">
							<a style="text-decoration:underline" href="<?php echo PATH ?>login/credentials/forgot">Forgot your credentials?</a>
						</div>
						
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
	
</div>


