<div class="whiteBackgroundContent">
	<div class="container" id="FormContent">
		<div class="row">
			<div class="col-md-12 breadCrumbs">
				<span><a href="<?php echo PATH ?>login">Login</a></span><i class="fa fa-caret-right" aria-hidden="true"></i><span>Forgot Credentials</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					Forgot Credentials
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				Enter your email address below to reset your password.
			</div>
		</div>
		<form id="ForgotCredentialsForm" method="post" action="<?php echo PATH ?>login/save/forgotcredentials">
			<?php echo $this -> form -> ValiationMessage("sendEmailError", "loadingEmail", "EmailErrorMessage") ?>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">Your Email</div>
						<input type="text" name="userEmailCheck" />
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<input type="submit" value="VALIDATE" class="redButton submitButton"  />
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
</div>

