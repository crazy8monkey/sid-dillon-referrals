<div class="whiteBackgroundContent">
	<div class="container" id="FormContent">	
		
		<?php if($_GET['token'] == $this -> userToken -> SecurityToken && $_GET['u'] == $this -> userToken -> GetMemberID()) : ?>		
			<div class="row">
				<div class="col-md-12">
					<div class="TitleHeader">
						New Password
					</div>
				</div>
			</div>
			<form id="NewPasswordForm" method="post" action="<?php echo PATH ?>login/save/password/<?php echo $_GET['u']; ?>">
				<?php echo $this -> form -> ValiationMessage("newPasswordError", "loadingPassword", "PasswordErrorMessage") ?>
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="inputLabel">New Password</div>
							<input type="password" name="userNewPassword" />
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine" style="padding-bottom: 50px;">
							<input type="submit" value="SAVE" class="redButton submitButton"  />
						</div>
					</div>
				</div>
				
			</form>
		<?php else: ?>
			This page has been expired
		<?php endif; ?>
	</div>
	
</div>

