<div class="whiteBackgroundContent">
	<div class="container" id="FormContent">
		<div class="row">
			<div class="col-md-12 breadCrumbs">
				<span><a href="<?php echo PATH ?>login">Login</a></span><i class="fa fa-caret-right" aria-hidden="true"></i><span>New Credentials</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="TitleHeader">
					New Credentials
				</div>
			</div>
		</div>
		
		<form id="CreateCredentials" method="post" action="<?php echo PATH ?>login/save/credentials">
			<?php echo $this -> form -> ValiationMessage("createCredentialError", "loadingCreate", "createErrorMessage") ?>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">Your Club ID (i.e. SD99999)</div>
						<input type="text" name="userClubID" id="userClubID" />
						<div style="margin:5px 0px;">
							<a href="javascript:void(0)" onclick="LoginController.ClubIDLookup()" style="text-decoration:underline">Forgot your Club ID?</a>	
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="inputLabel">New Username</div>
						<input type="text" name="newUsername" />
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" id="inputID1">
						<div class="inputLabel">New Password</div>
						<input type="password" name="newPassword" />
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine" style="padding-bottom: 50px;">
						<input type="submit" value="CREATE" class="redButton submitButton"  />
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
</div>
<?php echo $this -> clubIDLookup ?>

