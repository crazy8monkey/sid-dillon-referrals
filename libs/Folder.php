<?php

Class Folder {
	
	public static function DeleteUserDirectory($dir) {
		if ($handle = opendir($dir)) {
			$array = array();
 
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if(is_dir($dir.$file))
					{
					if(!@rmdir($dir.$file)) // Empty directory? Remove it
					{
						Folder::DeleteUserDirectory($dir.$file.'/'); // Not empty? Delete the files inside it
					}
				}
				else
				{
				@unlink($dir.$file);
				}
			}
		}
		closedir($handle);
		 
		@rmdir($dir);
		}
	}
	
	public static function CreateUserImageFolder($newFolder, $images) {
		//make a new directory folder
		$newReceipt = 'view/cms/purchase/receipt/' . $newFolder . '/';
		
		mkdir($newReceipt);
		//if new user recipe folder exists
		if(is_dir($newReceipt)) {
			foreach($images as $key => $value) {
				copy("view/purchase/product-images/" . $value, $newReceipt . $value);
				$kaboom = explode(".", $value);
				// Split file name into an array using the dot
				$ext = end($kaboom);
					
				Image::resizeImage($newReceipt . $value, $newReceipt . $value, 100, 100, $ext);
			}
		}	
	}
	
	public static function CreateWarrantyFolder($newFolder) {
		//make a new directory folder
		$newWarranty = 'view/cms/warranties/images/' . $newFolder . '/';
		
		mkdir($newWarranty);
		//if new user recipe folder exists
	}
	
	public static function CreateLocateLifeFolder($newFolder) {
		//make a new directory folder
		$newWarranty = 'view/cms/locatelife/location/' . $newFolder . '/';
		
		mkdir($newWarranty);
		//if new user recipe folder exists
	}

	
	
}
