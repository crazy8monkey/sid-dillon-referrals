<?php 

class View {
	
	
	public function __construct() {
		$this-> recordedTime = new Time();
		$this -> form = new Form();
		$this -> visible = New Permissions();
		$this -> security = new Security();
	}
	
	public function render($name, $noInclude = false) {

		if ($noInclude == true) {
			require 'view/' . $name . '.php';
		} else {
			require 'view/header.php';
			require 'view/' . $name . '.php';
			require 'view/footer.php';
		}
		
	}
	
	public function tooltip($msg) {
		return "<a href='javascript:void(0)' data-toggle='tooltip' data-placement='right' title='" . $msg . "' ><img src='" . PATH . "public/images/ToolTipHelp.png' style='margin: -5px 5px 5px 5px;' /></a>";
	}
	
	
}