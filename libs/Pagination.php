<?php

Class Pagination {
	private $mysqlCount;
	private $limit;
	private $targetpage;
	
	public $pagination;
	
	public function __construct() {}
	
	
	
	public function paginationLinks($setCount, $setLimit, $setTargetPage, $pageVariable, $status = false) {		
		$adjacents = 2;
		$page = isset($pageVariable) ? max((int)$pageVariable, 1) : 1;
		
		$statusString = "";
		$paginationLink = "";

		if($status) {
			$statusString = "&status=" . $status;	
		}

		if ($page == 0)
			$page = 1;
		//previous page is page - 1
		$prev = $page - 1;
		$leftArrow = "&#8249;";
		$rightArrow = "&#8250;";
		//next page is page + 1
		$next = $page + 1;
		$lastpage = ceil($setCount / $setLimit);
		$this->pagination = "";
		if ($lastpage > 1) {
			$this->pagination .= "<div class='pagination'>";
			//previous button
			if ($page > 1) {
				//leftRightPaginationLinks($targetPage, $leftRightArrow, $leftRightCounter, $isLink = false, $status = false)
				$this->pagination .= $this -> leftRightPaginationLinks($setTargetPage, $leftArrow, $prev, true, $status, "pagination-left");
				//$this->pagination .= "<a href='$setTargetPage?page=$prev$statusString'><div class='pagination-left'>&#8249;</div></a>";
			} else {
				//$this->pagination .= $this -> leftRightPaginationLinks($setTargetPage, $leftArrow, $prev, false, $status, "pagination-left");
				//$this->pagination .= "<a href='$setTargetPage?page=$prev'><div class='pagination-left'>&#8249;</div></a>";
			}
				
			//else
			//	$this->pagination .= "<div class='pagination-left'>&#8249;</div>";

			//pages
			if ($lastpage < 7 + ($adjacents * 2))//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page) {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
					} else {
						$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);						
					}
						
				}
			} elseif ($lastpage > 5 + ($adjacents * 2))//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
					
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);					
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $status, true);
					
				}
				//in middle; hide some front and some back
				elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $status, true);	
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					
					//$this->pagination .= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);
					$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, $lastpage, $status, true);					
				}
				//close to end; only hide early pages
				else {
					$this->pagination .= $this -> paginationLink($setTargetPage, 1, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, 2, $status, true);
					$this->pagination .= $this -> paginationLink($setTargetPage, "...", false, false);

					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page) {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, false, false, true);	
						} else {
							$this->pagination .= $this -> paginationLink($setTargetPage, $counter, $status, true);
						}
							
					}
				}
			}
			//next button
			if ($page < $counter - 1)
				$this->pagination .= $this -> leftRightPaginationLinks($setTargetPage, $rightArrow, $next, true, $status, "pagination-right");
			//else
			//	$this->pagination .= "<div class='pagination-right'>&#8250</div>";
			echo $this->pagination .= "<div style='clear:both'></div></div>\n";
		}
	    
	}
	
	private function leftRightPaginationLinks($targetPage, $leftRightArrow, $leftRightCounter, $isLink = false, $status = false, $className) {
		
		$leftRight = "";
		if($isLink == true) {
			if($status) {
				$leftRight .= "<a href='". $targetPage ."?page=" . $leftRightCounter . "&status=" . $status . "'><div class='pagination-left'>". $leftRightArrow . "</div></a>";	
			} else {
				$leftRight .= "<a href='". $targetPage ."?page=" . $leftRightCounter . "'><div class='pagination-left'>". $leftRightArrow . "</div></a>";
			}
		} else {
			$leftRight .= "<div class='". $className . "'>". $leftRightArrow . "</div>";
		}
		return $leftRight;
	}
	private function paginationLink($targetPage, $counter, $status = false, $isLink = false, $selected = false) {
		$paginationLink = "";
		if($isLink == true) {
			if($status) {
				$paginationLink .= "<a href='". $targetPage ."?page=". $counter ."&status=" . $status . "'><div class='pagination-number'>". $counter. "</div></a>";		
			} else {
				$paginationLink .= "<a href='". $targetPage ."?page=". $counter ."'><div class='pagination-number'>". $counter. "</div></a>";	
			}
		} else {
			if($selected == true) {
				$paginationLink .= "<div class='pagination-number pagination-number-selected'>" . $counter ."</div>";	
			} else {
				$paginationLink .= "<div class='pagination-number pagination-number-ellipse'>" . $counter ."</div>";
			}
			
		}
		
		
		return $paginationLink;
	}

	public function getPaginationLinks() {
		return $this-> pagination;	
	}
}