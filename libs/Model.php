<?php

class Model {


	private $cartItems;
	private $totalBoxes = array();
	private $totalCartWeight = null;
	
	public function __construct() {
		//database connection
		$this -> msg = new Message();
		$this -> json = new JSONparse();
		$this -> validate = new Validation();
		$this -> redirect = new Redirect();
		$this -> currentTime = new Time(); 
		$this -> pagination = new Pagination();
		$this -> settings = Settings::GetSettings();
        $this -> setShippingCompany();
		$this -> db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
	}

	public function setShippingCompany($overideCompany = false) {
        $company = $this->settings->shippingCompany;
        if($overideCompany){
            $company = $overideCompany;
        }

        if($company == "UPS"){
            $this -> shipping = new UPS($this -> settings -> getCompanyName(),
                $this -> settings -> getCompanyAddress(),
                $this -> settings -> getCompanyCity(),
                $this -> settings -> getCompanyState(),
                $this -> settings -> getCompanyZip(),
                $this -> settings -> getCompanyPhoneNumber());
        } else {
            $this -> shipping = new FedEx($this -> settings -> getCompanyName(),
                $this -> settings -> getCompanyAddress(),
                $this -> settings -> getCompanyCity(),
                $this -> settings -> getCompanyState(),
                $this -> settings -> getCompanyZip(),
                $this -> settings -> getCompanyPhoneNumber());
        }
		

	}
	
	private function getClosestBoxSize($numPackages){
        $shippingBoxes = new ShipmentBoxes();
        //echo 'Num Items To Fit: ' . $numPackages;

        $closestBox = 4096;
        $largestBox = -1;

        foreach($shippingBoxes -> TotalShippingBoxes() as $box) {
            //echo 'Box.ItemsHold' . $box->itemsHold;
            if ($box->itemsHold == $numPackages) {
                return $numPackages; // Perfect fit. Use it.
            } else {
                if ($box->itemsHold > $numPackages) { // Box can fit all bottles. See if it is the smallest one that can do this.
                    if ($box->itemsHold < $closestBox) { // Box is smaller than current smallest that can hold all.
                        $closestBox = $box->itemsHold;
                    }
                } else { // Box is too small. See if it is the largest box that is too small
                    if ($box->itemsHold > $largestBox) {
                        $largestBox = $box->itemsHold;
                    }
                }
            }
        }

        if($closestBox == 4096){ // closest box was never specified. Use the largest box (which will be smaller than what we need)
            return $largestBox;
        }else{
            return $closestBox; // Closest box was specified. Use it. (This will be larger than what we need).
        }

    }

	public function getTotalItems() {
		

		$cartProductInfo = array();
		
		$bigItemCount = null;
		$finalItemCount = null;
		//$this -> totalBoxes
		$totalProductArray = array();
		
		// get weight of each item
		foreach($_SESSION['cart_array'] as $item) {
			
			$product = Product::withID($item['product_ID']);
						
			$quantityStart = 1;
			
			while($quantityStart <= $item['quantity']) {
				//echo "Product ID: " . $item['product_ID'] . " / product weight: " . $product -> weight . "<br />";
				$quantityStart++;
				
				array_push($cartProductInfo, array('productID' => $item['product_ID'],
											       'weight' => $product -> weight));
			}
			$this -> cartItems += $item['quantity'];
		}
		
		
		$unPackagedItems = $this -> cartItems;
		

		
		
		while ($unPackagedItems > 0) {
            $largestNumber = $this->getClosestBoxSize($unPackagedItems);
			$finalItemCount = '';
			
			//get box that can hold the largest 'items hold' value

			$boxDimentions = ShipmentBoxes::WithLargestItem($largestNumber);
			
			
			
			$productWeightPackage = 0;
			
			
				$min = min ( count ($cartProductInfo), $largestNumber);
    			
				$max = count($cartProductInfo);
				$i = 0;
				
				
				
				foreach($cartProductInfo as $weight) {
					//echo 'product id again: ' . $weight['productID'] . ' / weight: '. $weight['weight'] . "<br />";
					$productWeightPackage += $weight['weight'];
					
					//echo $i . "<br />";
					unset($cartProductInfo[$i]);
					$i++;
					if(count($cartProductInfo) !=0) {
						if ($i == $min) {
		      				//$i = 0;
		      			//$productWeightPackage = 0;
		      				break;
						//	echo "<br />";
		      			}	
					}
				}	
				//re indexes the cartProductInfo array
				$cartProductInfo = array_values($cartProductInfo);
				//echo "i value after loop: " . $i . "<br />";
			
				//echo 'package weight: ' . $productWeightPackage . " lbs <br />";
			
			

			
		
			//echo "Un-packaged items is: " . $unPackagedItems . " / "  .  $finalItemCount . " items can hold <br />";
			array_push($this -> totalBoxes, array("length" => $boxDimentions -> length,
                                                  "width" => $boxDimentions -> width,
                                                  "height" => $boxDimentions -> height,
                                                  "weight" => ((int)$boxDimentions -> weight) + ((int)$productWeightPackage)));
												  //
					
			$unPackagedItems -= $largestNumber;
			$this -> totalCartWeight += $boxDimentions -> weight + $productWeightPackage;
			//$totalCartWeight += ((int)$boxDimentions -> weight) + ((int)$productWeightPackage));
			//echo $finalItemCount . "<br />";    
		} 

		//generate total shipping boxes
		//$this -> totalBoxes
		//echo $finalItemCount . " largest item contains <br />";
		//echo $this -> cartItems . " items <br />";
		//echo print_r($cartProductInfo) . "<br />";
		//echo print_r($this -> totalBoxes) . "<br />";
		
	}
	
	public function getTotalCartWeight() {
		return $this -> totalCartWeight;
	}
	
	public function getTotalBoxes() {
		return $this -> totalBoxes;
	}




}