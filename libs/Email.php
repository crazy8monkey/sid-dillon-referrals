<?php

Class Email {
			
	public $to;
	public $subject;
	private $msg;
	private $header;
	
	public function __construct() {
		$this -> header = 'From: Sid Dillon Referrals <SidDillonReferrals@donotreply.com>'. "\r\n";
		$this -> header .= 'MIME-Version: 1.0' . "\r\n";
		$this -> header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		$this -> header .= 'Bcc: netadmin@siddillon.com, adam.schmidt@siddillon.com' . "\r\n";	
	}
	
	private function emailLogo() {
		return "<img src='" . PATH . "public/images/SidDillonLogoEmail.png' />";
	}

	public function NewReferral($content) {
		
		$this -> msg = "<html style='padding:0px; margin:0px;'><body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
							<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
								<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
								<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Welcome to our Referral Program!</td></tr>
								<tr><td style='padding:20px 30px; background:white; border-bottom:1px solid #ececec'>
										<table cellpadding='0' cellspacing='0'>
											<tr><td style='font-family:arial; font-size:25px'>Your Information</td></tr>
											<tr><td style='font-size:25px; padding:15px 0px 0px 0px'>
													<table>
														<tr>
															<td style='font-family:arial; font-weight:bold; font-size:16px; width:120px'>Club ID:</td>
															<td style='font-family:arial; font-size:16px'>SD" . $content['referral-id'] . "</td>
														</tr>
														<tr>
															<td style='font-family:arial; font-weight:bold; font-size:16px; width:120px'>Username:</td>
															<td style='font-family:arial; font-size:16px'>" . $content['username'] . "</td>
														</tr>
														<tr>
															<td style='font-family:arial; font-weight:bold; font-size:16px; width:120px'>Password:</td>
															<td style='font-family:arial; font-size:16px'>" . $content['password'] . "</td>
														</tr>
													</table>
												</td></tr>
											</table>
										</td>
									</tr>
									<tr><td style='padding:20px 30px; background:white; font-family:arial; font-size:16px'>
											Sid Dillon knows that our best form of advertising is you, our satisfied body of 
											customers! Realizing this, we have developed the Sid Dillon Referral Program to 
											thank you for sharing your positive experiences with others and recommending us to them.
										</td></tr>
									<tr><td style='padding:0px 30px 20px 30px; background:white; font-family:arial; font-size:16px'>
											<table cellpadding='0' cellspacing='0' style='margin:0 auto;'>
												<tr>
													<td style='padding:0px 10px 0px 0px'>
														<a href='" . PATH . "login' style='color:white;'><div style='background: #a81e28; color: white; width: 200px; text-align: center; font-size: 16px; padding: 15px 0px;'>LOGIN</div></a>
													</td>
													<td style='padding:0px;'>
														<a href='". PATH . "submit' style='color:white;'><div style='background: #a81e28; color: white; width: 200px; text-align: center; font-size: 16px; padding: 15px 0px;'>SUBMIT A REFERRAL</div></a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}

	public function UserNewCredentials($content) {
		
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>Your User Credentials</td>
									</tr>
									<tr>
										<td style='padding:30px; background:white'>
											<table style='margin:0 auto;' cellpadding='0' cellspacing='0' width='350' align='center'>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px'>Club ID:</td>
													<td style='font-family:arial; font-size:25px'>" .$content['club-id'] . "</td>
												</tr>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px'>Username:</td>
													<td style='font-family:arial; font-size:25px'>" .$content['username'] . "</td>
												</tr>
												<tr>
													<td style='font-weight:bold; font-family:arial; font-size:25px; padding:0px 0px 25px 0px'>Password:</td>
													<td style='font-family:arial; font-size:25px; padding:0px 0px 25px 0px'>" . $content['password'] . "</td>
												</tr>
												<tr>
													<td colspan='2' style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . PATH . "login' style='text-decoration:underline; color:white;'>
															Login
														</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	
	public function PasswordReset($link) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr>
										<td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td>
									</tr>
									<tr>
										<td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial;'>Reset Your Password</td>
									</tr>
									<tr>
										<td style='padding:30px;'>
											<table cellpadding='0' cellspacing='0'>
												<tr>
													<td style='font-family:arial; text-align:center; padding:0px 0px 20px 0px; font-size:20px;'>Click on the link below to reset your Sid Dillon Referral password.</td>
												</tr>
												<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='" . $link . "' style='text-decoration:underline; color:white;'>Reset your Password</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function SaleRepNewReferral($content) {
			$this -> msg = "<html style='padding:0px; margin:0px;'><body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='400' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>You have a new referral</td></tr>
									<tr><td style='padding:20px 30px; background:white; border-bottom:1px solid #ececec'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-family:arial; font-size:25px'>Submitted By</td></tr>
												<tr><td style='font-size:25px; padding:15px 0px 0px 0px'>
														<table>
															<tr><td style='font-family:arial; font-size:16px; padding:0px 0px 15px 0px'><strong>Club ID:</strong><br />". $content['sd-number']  . "</td></tr>
															<tr><td style='font-family:arial; font-size:16px;'><strong>Full Name:</strong><br />" . $content['member-full-name']  . "</td></tr>
														</table>
													</td>
												</tr>
											</table>
										</td></tr>
									<tr><td style='padding:20px 30px; background:white; font-family:arial; font-size:16px'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-family:arial; font-size:25px'>Referral Info.</td></tr>
												<tr><td style='font-size:25px; padding:15px 0px 0px 0px'>
														<table>
															<tr><td style='font-family:arial; font-size:16px; padding:0px 0px 15px 0px'><strong>" . $content['referral-full-name'] . "</strong><br />" . $content['referral-phone'] . "<br />" . $content['referral-email'] . "</td></tr>
															<tr><td style='font-family:arial; font-size:16px;'><strong>Desired Vehicle:</strong><br />" . $content['referral-desired-vehicle'] . "<br /><br /><strong>Exp. Date of Purchase:</strong><br />" . $content['referral-expected-purchase'] . "</td></tr>
														</table>
													</td></tr>
											</table>
										</td></tr>";
									if(!empty($content['referral-notes'])) {
										$this -> msg .= "<tr><td style='padding:20px 30px; background:white; font-family:arial; font-size:16px; border-top:1px solid #ececec'><strong>Notes</strong><br />" . $content['referral-notes'] . "</td></tr>";
									}
									
								$this -> msg .= "<tr>
													<td style='font-family:arial; color:white; background: #1e5fa8; text-align:center; padding:10px;'>
														<a href='http://internal.siddillon.com/' style='text-decoration:underline; color:white;'>Login</a>
													</td>
												</tr></table></body></html>";
			
		
		$this -> SendEmail();
	}
	
	
	public function PaidReferral($content) {
		$this -> msg = "<html style='padding:0px; margin:0px;'>
							<body style='background:#f5f5f5; padding:30px 0px 30px 0px; margin:0px;'>	
								<table style='background:white; margin:0 auto;' cellpadding='0' cellspacing='0' width='500' align='center'>
									<tr><td style='background:black; font-family: arial; text-align:center; padding:30px 10px 10px 10px'>" . $this -> emailLogo() ."</td></tr>
									<tr><td style='padding: 15px; background: #1e5fa8; font-size: 30px; color: white; font-family: arial; text-align:center'>Paid Referral</td></tr>
									<tr>
										<td style='padding:20px 30px; background:white;'>
											<table cellpadding='0' cellspacing='0'>
												<tr><td style='font-size:16px; padding:15px 0px 0px 0px; font-family:arial'>
														<table cellpadding='0' cellspacing='0'>
															<tr><td style='padding:0px 10px 10px 0px'><strong>Todays Date:</strong></td><td style='padding:0px 0px 10px 0px'>". $content['todays-date'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>New Paid Referral:</strong></td><td>RF". $content['id'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>Total Paid Referrals this year:</strong></td><td>". $content['win-count'] . "</td></tr>
															<tr><td style='padding:0px 10px 0px 0px'><strong>Paid Amount:</strong></td><td>$". $content['win-amount'] . "</td></tr>
														</table>
													</td></tr>
											</table>
										</td></tr>
								</table>
							</body>
						</html>";
		
		$this -> SendEmail();
	}
	
	public function ReferralError($content) {
		$this -> msg = "<table cellpadding='0' cellspacing='5'>" . $content ."</table>";
		$this -> SendEmail();
	}
		
	private function SendEmail() {
		mail($this -> to, $this -> subject . EMAIL_SUBJECT, $this -> msg, $this -> header);
	}
	
	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}
}