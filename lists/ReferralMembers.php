<?php

class ReferralMembers extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function getReferralMemberCLUBIDs() {
		return $this -> db -> select('SELECT ClubID FROM referralmembers');
	}
	

}