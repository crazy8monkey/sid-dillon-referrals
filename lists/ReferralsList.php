<?php

class ReferralsList extends BaseObjectList {
    //status section	
	//0 = active
	//1 = sold
	//2 = paid	
	//3 = inactive
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function getActiveReferralsByMember($memberID) {
		$activeReferrals = $this -> db -> prepare('SELECT * FROM referrals WHERE Status IN (0, 1) AND submittedDate >= :startYear AND submittedDate <= :endYear AND memberID = :memberID');
		$activeReferrals -> execute(array(':memberID' => $memberID, 
		        					    ':startYear' => $this -> time -> StartOfYearSQL(), 
		        					    ':endYear' => $this -> time -> EndOfYearSQL()));
		
		return $activeReferrals -> fetchAll();		
	}
	
	public function getPaidReferralsByMember($memberID) {
		$paidReferrals = $this -> db -> prepare('SELECT * FROM referrals WHERE Status = 2 AND submittedDate >= :startYear AND submittedDate <= :endYear AND memberID = :memberID ORDER BY PaidAmount DESC');
		$paidReferrals -> execute(array(':memberID' => $memberID, 
		        					    ':startYear' => $this -> time -> StartOfYearSQL(), 
		        					    ':endYear' => $this -> time -> EndOfYearSQL()));
		
		return $paidReferrals -> fetchAll();
	}
	
	
	
	public function getInactiveReferralsByMember($memberID) {
		$inActiveReferrals = $this -> db -> prepare('SELECT * FROM referrals WHERE Status = 3 AND submittedDate >= :startYear AND submittedDate <= :endYear AND memberID = :memberID');
		$inActiveReferrals -> execute(array(':memberID' => $memberID, 
		        					    ':startYear' => $this -> time -> StartOfYearSQL(), 
		        					    ':endYear' => $this -> time -> EndOfYearSQL()));
		
		return $inActiveReferrals -> fetchAll();					
	}

}