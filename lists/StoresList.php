<?php

class StoresList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function getReferralStores() {
		return $this -> db -> select('SELECT * FROM stores WHERE isReferralClubStore = 1');
	}
	

}