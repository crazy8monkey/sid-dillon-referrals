var Globals = function() {
	
	var elementFillOnResponse
	
	function closeMessaging() {
		$('.message').fadeOut('fast');
	}
	function hideFormOverlay() {
		$(".form-overlay").fadeOut("fast");
	}
	
	function ClosePopups() {
		$(".Overlay, .WhitePopupForm").fadeOut("fast");
	}
	
	function ValidateClubIDLookup(ElementFill) {
		$("#clubIDLookupForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			elementFillOnResponse = ElementFill

			Globals.AjaxPost(formURL, postData, "#loadingClubID", ClubIDLookupResponse)		
		});
	}
	
	function ClubIDLookupResponse(response) {
		if(response.errorMessage) {
			$("#ClubIDLookupError").show();
			$("#loadingClubID").fadeOut("slow");
			$("#ClubIDLookupMessage").fadeTo(0, 100);
			$("#ClubIDLookupError").css("height", "33px");
			$("#ClubIDLookupMessage").addClass("alert-danger");
			$("#ClubIDLookupMessage").html(response.errorMessage);
			$("#ClubIDLookupMessage").delay(1500).fadeTo(3500, 0);
		}
		
		if(response.success) {
			$("#ClubIDLookup, .Overlay").fadeOut('fast', function() {
				$(elementFillOnResponse).val(response.success);	
			});
		}
	}
	
	function ShowErrorMessage(Element) {
		$(Element).show();
		$(Element).css("height", "36px");
	}
	
	function LoadingIndicator(Element) {
		$(Element).fadeOut("slow");
	}
	
	function ShowResponseMessage(Element, type, message) {
		switch(type) {
			case "error":
				$(Element).addClass("alert-danger");
				$(Element).removeClass("alert-success");
				break;
			case "success":
				$(Element).addClass("alert-success");
				$(Element).removeClass("alert-danger");
				break;
			
		}
		$(Element).fadeTo(0, 100);	
		$(Element).html(message);
		$(Element).delay(1500).fadeTo(3500, 0);
		
	}
	
	function redirect(string, delay) {
		if(delay) {
			setTimeout(function(){
				window.location.assign(string); 
			}, delay);
		} else {
			window.location.assign(string);
		}
	}
	
	function ToggleMobileMenu() {
		$(document).click(function (e) {
            if ($(e.target).closest('#hrefMobileMenu').length === 0) {
            	$("header .mobileView .menu").hide();
        	}
        });
			
		$("header .mobileView .menu").toggle();
	}
	
	function ToggleReferralMenu() {
		$(document).click(function (e) {
            if ($(e.target).closest('#hrefReferralMenu').length === 0) {
            	$(".MainMenu ul").hide();
        	}
        });
			
		$(".MainMenu ul").toggle();
	}
	
	function ToolTipMessage(msg) {
		
	}
	
	return {
		redirect: redirect,
		AjaxPost: function(formURL, postData, loadingElement, success) {
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: postData,
		        dataType: 'json',
				cache: false,
		        beforeSend: function(){
		            $(loadingElement).show();
    		    },
				success : success
			});	
		},
		removeValidationMessage: function(ID) {
			$("#inputID" + ID + " .errorMessage").html("");
			$("#inputID" + ID + "Name").remove()
		},
		ScrollToTop: function(element) {
			$('html, body').animate({
		        scrollTop: $(element).offset().top - 200
		    },1000);
		    $(".ajaxLoadingIndicator").hide();
		},
		ClosePopups: ClosePopups,
		ValidateClubIDLookup: ValidateClubIDLookup,
		ShowErrorMessage: ShowErrorMessage,
		LoadingIndicator: LoadingIndicator,
		ShowResponseMessage: ShowResponseMessage,
		ToggleMobileMenu: ToggleMobileMenu,
		ToggleReferralMenu: ToggleReferralMenu,
		ToolTipMessage: ToolTipMessage
	}	
	
}();