var MemberController = function() {
	
	jQuery.fn.extend({
	    toggleText: function (a, b){
	        var that = this;
	            if (that.text() != a && that.text() != b){
	                that.text(a);
	            }
	            else
	            if (that.text() == a){
	                that.text(b);
	            }
	            else
	            if (that.text() == b){
	                that.text(a);
	            }
	        return this;
	    }
	});
	
	
	function CreateCredentialsResponses(response) {
		if(response.errorMessage) {
			$("#errorMesssages").show();
			$(".loadingIndicator").fadeOut("slow");
			$("#messages").fadeTo(0, 100);
			$("#errorMesssages").css("height", "36px");
			$("#messages").addClass("alert-danger");
			$("#messages").html(response.errorMessage);
			$("#messages").delay(1500).fadeTo(3500, 0);
		}
		
		if(response.credentialSaveSuccess) {
			$("#errorMesssages").show();
			$(".loadingIndicator").fadeOut("slow");
			$("#messages").removeClass("alert-danger");
			$("#messages").addClass("alert-success");
			
			$("#messages").fadeTo(0, 100);
			$("#messages").html(response.credentialSaveSuccess.msg);
			
			setTimeout(function(){
				Globals.redirect(response.credentialSaveSuccess.redirect);  
			}, 3000);
			
			
		}	
	}
	
	function ProfileSaveResponses(response) {
		if(response.ValidationErrors) {
			$("#validationSummaryMessageElement").fadeIn();
			
			$("#messageTypeColor").removeClass("alert-success");
			$("#messageTypeColor").addClass("alert-danger");
			
			var ValidationMessageList = '';
			var validationMessagesArray = response.ValidationErrors;
			
			var ValidationErrors = response.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   
			    ValidationMessageList += "<div class='inputError' id='inputID" + validationMessagesArray[i].inputID + "Name'>- " + validationMessagesArray[i].inputType + "</div>";
			    if(validationMessagesArray[i].inputID !== '') {
			    	$("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(" | " + validationMessagesArray[i].errorMessage);	
			    }
			    
			    		    
			}
			Globals.ScrollToTop("#validationSummaryMessageElement");
			$("div#divErrorHeader").html("There are some issues updating your profile")
			$(".LoadingReferral").hide();
			$("span#validationSummaryMessageText").html(ValidationMessageList)			
		}
		
		if(response.success) {
			$("#validationSummaryMessageElement").fadeIn();
			$("#messageTypeColor").removeClass("alert-danger");
			$("#messageTypeColor").addClass("alert-success");
			$("div#divErrorHeader").html("Profile Updated");
			Globals.ScrollToTop("#validationSummaryMessageElement");
		}
	}
	
	function InitializeCreateCredentials() {
	    $("#CreateCredentials").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ".loadingIndicator", CreateCredentialsResponses)		
		});
	}
	
	function InitializeProfile() {
		$("#ProfileUpdate").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ".loadingIndicator", ProfileSaveResponses)		
		});
		
		$("#PhoneInput").mask("(999) 999-9999",{placeholder:" "});
	}
	
	function getReferralSingle(referralID, type) {
		$.getJSON( "../member/GET/" + referralID, function( data ) {
			//submitted date configuration
			var SubmittedDate = data.submittedDate.split('-');
					
			var year = SubmittedDate[0];
			var month = SubmittedDate[1];
			var day = SubmittedDate[2];
			$("#submittedDate").html(month + "/" + day + "/" + year);
			
			switch(type) {
				case "sold":
					//sold date
					$("#soldLine").show();
					
					var soldDateValue = data.SoldDate.split('-');
					
					var soldYear = soldDateValue[0];
					var soldMonth = soldDateValue[1];
					var soldDay = soldDateValue[2];
					
					$("#SoldDateContent").html(soldMonth + "/" + soldDay + "/" + soldYear);
					
					break;
					
				case "active":
					$("#soldLine").hide();
					break;
				
				case "inactive":
					$("#soldLine").hide();
					break;
			}
			
			
					
			if(data.ReferralNotes != null) {
				$("#ReferralMemberNotesContent").html(data.ReferralNotes);
			} else {
				$("#ReferralMemberNotes").hide();
			}
			
			$("#SalesPerson").html(data.salesperson);
			$("#storeName").html(data.storeName);
			
			$(".Overlay, #ReferalDetail").fadeIn("fast");
		});
	}
	

	function toggleReferralTierInfo() {
		$("#TierList").toggle();
		$(".referralTierPlus").toggleText('-', '+');
		$("#RulesRegulationsText").hide();
		$(".referralRulesText").html('+');
	}
	
	function toggleRulesRegulationsInfo() {
		$("#RulesRegulationsText").toggle();
		$(".referralRulesText").toggleText('-', '+');
		$(".referralTierPlus").html('+');
		$("#TierList").hide();
	}
	
	return {
		InitializeCreateCredentials: InitializeCreateCredentials,
		InitializeProfile: InitializeProfile,
		getReferralSingle: getReferralSingle,
		toggleReferralTierInfo: toggleReferralTierInfo,
		toggleRulesRegulationsInfo: toggleRulesRegulationsInfo
	}
}();
