var RegisterController = function() {
	
	function chk_scroll(e) {
	    var elem = $(e.currentTarget);
	    if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
	        $(".agreeBtn").removeClass("disabled");
	        $(".agreeBtnLink").removeAttr("disabled");
	        $(".agreeBtnLink").attr("onclick", "RegisterController.hideTermsConditions()");
	    }
	
	}
	
	function RegisterResponses(data) {
		if(data.ValidationErrors) {
			$("#validationSummaryMessageElement").fadeIn();
			
			var ValidationMessageList = '';
			var validationMessagesArray = data.ValidationErrors;
			
			var ValidationErrors = data.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   
			    ValidationMessageList += "<div class='inputError' id='inputID" + validationMessagesArray[i].inputID + "Name'>- " + validationMessagesArray[i].inputType + "</div>";
			    if(validationMessagesArray[i].inputID !== '') {
			    	$("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(" | " + validationMessagesArray[i].errorMessage);	
			    }
			    
			    		    
			}
			Globals.ScrollToTop("#validationSummaryMessageElement");
			$("div#divErrorHeader").html("There are some issues when filling out your registration.<br />Please correct the following input fields")
			$(".LoadingRegistration").hide();
			$("span#validationSummaryMessageText").html(ValidationMessageList)			
		}
		
		if(data.MySqlError) {
			$("#validationSummaryMessageElement").fadeIn();
			$("div#divErrorHeader").html("")
			scrollToTop("#validationSummaryMessageElement");
			$(".LoadingRegistration").hide();
			$("span#validationSummaryMessageText").html(data.MySqlError)
		}
		
		if(data.success) {
			$(".LoadingRegistration").hide();
			$("#RegisterToClubForm").fadeOut('fast', function() {
				$("#ThankYouClubContent").fadeIn('fast');	
			});
				
		}


	}
	
	function Initialize() {
	    $("#newRegisterUser").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ".LoadingRegistration", RegisterResponses)		
		});
		
		$("#TermsAndConditions .Content").bind('scroll',chk_scroll);
				
		
		//http://digitalbush.com/projects/masked-input-plugin/#demo
		$("#phoneInput").mask("(999) 999-9999",{placeholder:" "});

	}
	
	function removeValidationMessage(ID) {
		$("#inputID" + ID + " .errorMessage").html("");
		$("#inputID" + ID + "Name").remove()
	}
	
	function showTermsConditions(element, ID) {
		
		if(element.checked) {
			$("#TermsAndConditions, .Overlay").fadeIn('fast');	
			$("#inputID" + ID + " .errorMessage").html("");
			$("#inputID" + ID + "Name").remove()
		}		
	}
	
	function hideTermsConditions() {
		$("#TermsAndConditions, .Overlay").fadeOut('fast');
	}
	
	function ChangeStateValue(element, ID) {
		$("#hiddenState").val($(element).val());
		$("#inputID" + ID + " .errorMessage").html("");
		$("#inputID" + ID + "Name").remove()
	}
	
	return {
		Initialize: Initialize,
		showTermsConditions: showTermsConditions,
		hideTermsConditions: hideTermsConditions,
		ChangeStateValue: ChangeStateValue 
	}
}()
