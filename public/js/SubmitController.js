var SubmitController = function() {
	
	function ReferralResponses(response) {
		if(response.ValidationErrors) {
			$("#validationSummaryMessageElement").fadeIn();
			
			var ValidationMessageList = '';
			var validationMessagesArray = response.ValidationErrors;
			
			var ValidationErrors = response.ValidationErrors.length;
			for (var i = 0; i < ValidationErrors; i++) {
			   
			    ValidationMessageList += "<div class='inputError' id='inputID" + validationMessagesArray[i].inputID + "Name'>- " + validationMessagesArray[i].inputType + "</div>";
			    if(validationMessagesArray[i].inputID !== '') {
			    	$("#inputID" + validationMessagesArray[i].inputID + " .errorMessage").html(" | " + validationMessagesArray[i].errorMessage);	
			    }
			    
			    		    
			}
			Globals.ScrollToTop("#validationSummaryMessageElement");
			$("div#divErrorHeader").html("There are some issues when filling out your registration.<br />Please correct the following input fields")
			$(".LoadingReferral").hide();
			$("span#validationSummaryMessageText").html(ValidationMessageList)			
		}
		
		if(response.MySqlError) {
			$("#validationSummaryMessageElement").fadeIn();
			$("div#divErrorHeader").html("")
			Globals.ScrollToTop("#validationSummaryMessageElement");
			$(".LoadingRegistration").hide();
			$("span#validationSummaryMessageText").html(response.MySqlError)
		}
		
		if(response.success) {
			$("#SubmitReferralForm").fadeOut('fast', function() {
				$("#ThankYouReferral").fadeIn("fast");
				$(".LoadingReferral").hide();
			})
		}
		
	}
	
	function Initialize() {
	    $("#NewReferralForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, ".LoadingReferral", ReferralResponses)		
		});
		
		//http://digitalbush.com/projects/masked-input-plugin/#demo
		$("#ExpectedDate").datepicker();
		$("#PhoneInput").mask("(999) 999-9999",{placeholder:" "});

	}
	
	function ShowSalesmanDropDown(element, errorMessageID) {
		var storeID = $(element).val();
		
		$("#inputID" + errorMessageID + " .errorMessage").html("");
		$("#inputID" + errorMessageID + "Name").remove()
		
		
		var salesman = [];
		if(storeID == 0) {
			$("#SalesManSelect").hide();
		} else {
			$("#SalesManSelect").show();
			$("#salesManDropDown").html("<option value='0' id='EmptySalesmanOption'>Select Salesperson</option>");
			//<option value="0" id="EmptySalesmanOption">Select Salesperson</option>
			
			$.getJSON( "submit/GET/" + storeID, function( data ) {
				$.each( data, function( key, val ) {
					salesman.push( "<option value='" + val.userID + "'>" + val.firstName + " " + val.lastName + "</option>" );
				});
				salesman.push("<option value='other'>Other</option>")
				
				$(salesman.join("")).insertAfter( "#EmptySalesmanOption" ); 
			});
				
		}
	}
	
	function ClubIDLookup() {
		//var windowWidth = $(window)
		//if(windowWidth > 680) {
			$(".Overlay, #ClubIDLookup").fadeIn('fast');	
		//} else {
		//	$("#ClubIDLookup").show()
		//	$("#ClubIDLookup").slideUp('fast');
		//}
		
				
	}
	
	return {
		Initialize: Initialize,
		ShowSalesmanDropDown: ShowSalesmanDropDown,
		ClubIDLookup: ClubIDLookup
	}
}()
