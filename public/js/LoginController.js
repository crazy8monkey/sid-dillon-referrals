var LoginController = function() {
	
	function CreateCredentialsResponses(response) {
		if(response.errorMessage) {
			Globals.ShowErrorMessage("#createCredentialError");
			Globals.LoadingIndicator("#loadingCreate");
			Globals.ShowResponseMessage("#createErrorMessage", "error", response.errorMessage)
		}
		
		if(response.credentialSaveSuccess) {
			Globals.ShowErrorMessage("#createCredentialError");
			Globals.LoadingIndicator("#loadingCreate");
			Globals.ShowResponseMessage("#createErrorMessage", "success", response.credentialSaveSuccess.msg)
			
			Globals.redirect(response.credentialSaveSuccess.redirect, 3000);  
		}
	}
	
	
	function UserLoginResponses(response) {
		if(response.errorMessage) {
			Globals.ShowErrorMessage("#loginError");
			Globals.LoadingIndicator("#loginLoading");
			Globals.ShowResponseMessage("#loginErrorMessage", "error", response.errorMessage);
		}
		
		if(response.deactivatedAccount) {
			Globals.ShowErrorMessage("#loginError");
			Globals.LoadingIndicator("#loginLoading");
			$("#loginErrorMessage").html(response.deactivatedAccount);
			//Globals.ShowResponseMessage("#loginErrorMessage", "error", response.deactivatedAccount);
		}
		
		if(response.redirect) {
			Globals.redirect(response.redirect); 
		}
	}
	
	function ForgotCredentialsResponse(response) {
		if(response.errorMessage) {
			Globals.ShowErrorMessage("#sendEmailError");
			Globals.LoadingIndicator("#loadingEmail");
			Globals.ShowResponseMessage("#EmailErrorMessage", "error", response.errorMessage);
		}
		
		if(response.success) {
			Globals.ShowErrorMessage("#sendEmailError");
			Globals.LoadingIndicator("#loadingEmail");
			Globals.ShowResponseMessage("#EmailErrorMessage", "success", response.success.msg);
			
			Globals.redirect(response.success.redirect, 3000);  
		}		
	}
	
	function ResetResponse(response) {
		if(response.errorMessage) {
			Globals.ShowErrorMessage("#newPasswordError");
			Globals.LoadingIndicator("#loadingPassword");
			Globals.ShowResponseMessage("#PasswordErrorMessage", "error", response.errorMessage);
		}
		if(response.success) {
			Globals.ShowErrorMessage("#newPasswordError");
			Globals.LoadingIndicator("#loadingPassword");
			Globals.ShowResponseMessage("#PasswordErrorMessage", "success", response.success.msg);
						
			Globals.redirect(response.success.redirect, 3000);
		}
	}
	
	function InitializeCreateCredentials() {
	    $("#CreateCredentials").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#loadingCreate", CreateCredentialsResponses)		
		});
	}
	
	function InitializeLogin() {
	    $("#LoginForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#loginLoading", UserLoginResponses)		
		});		
	}
	
	function InitializeForgotCredentials() {
		$("#ForgotCredentialsForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#loadingCreate", ForgotCredentialsResponse)		
		});
	}
	
	function InitializeReset() {
		$("#NewPasswordForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			Globals.AjaxPost(formURL, postData, "#loadingPassword", ResetResponse)		
		});
	}
	
	
	function ClubIDLookup() {
		$(".Overlay, #ClubIDLookup").fadeIn('fast');
	}
	
	return {
		InitializeCreateCredentials: InitializeCreateCredentials,
		InitializeLogin: InitializeLogin,
		InitializeForgotCredentials: InitializeForgotCredentials,
		InitializeReset: InitializeReset,
		ClubIDLookup: ClubIDLookup
	}
}();
