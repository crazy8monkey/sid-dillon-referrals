<?php

class Member extends Controller {

	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage(PATH. 'login');
			exit();
		}
		
		$this -> view -> memberLoginSection = "";
		$this -> view -> showMemberButtons = "";
		
		
		$this -> view -> css = array('MemberController.css');
		$this -> view -> js = array(PATH . "public/js/MemberController.js");
	}
			
	public function dashboard() {
		Session::init();
		$referralMember = ReferralMember::WithID(Session::get('user'));
		$Referrallist = new ReferralsList();
		
		$this -> view -> title = "Dashboard" . parent::SIDDILLONTITLE;
		$this -> view -> MemberInfo = $referralMember;
		$this -> view -> EarnedReferrals = $Referrallist -> getPaidReferralsByMember($referralMember -> ClubID);
		$this -> view -> ActiveReferrals = $Referrallist -> getActiveReferralsByMember($referralMember -> ClubID);
		$this -> view -> InactiveReferrals = $Referrallist -> getInactiveReferralsByMember($referralMember -> ClubID);
		$this -> view -> render('member/dashboard');
	}
	
	
	public function profile() {
		Session::init();
		$member = ReferralMember::WithID(Session::get('user'));
		$this -> view -> title = "Your Profile" . parent::SIDDILLONTITLE;
		$this -> view -> profileInfo = $member;
		$this -> view -> startJsFunction = array('MemberController.InitializeProfile();');
		$this -> view -> states = $member -> States();
		$this -> view -> render('member/profile');
	}
	
	public function rules() {
		$tiers = new ReferralTierList();
		
		$this -> view -> title = "Rules & Regulations" . parent::SIDDILLONTITLE;
		$this -> view -> referralTiers = $tiers -> getReferralTiers();
		$this -> view -> render('member/rules');
	}
	
	public function GET($id) {
		echo json_encode(ReferralSingle::WithID($id));
	}
	
	public function save($type) {
		switch($type) {
			case "profile":
				Session::init();
				
				$member = ReferralMember::WithID(Session::get('user'));				
				$member -> firstName = $_POST['firstName'];
				$member -> lastName = $_POST['lastName'];
				$member -> email = $_POST['email'];
				$member -> Phone = $_POST['phone'];
				$member -> address = $_POST['address'];
				$member -> city = $_POST['city'];
				$member -> State = $_POST['hiddenState'];
				$member -> zip = $_POST['Zip'];
				$member -> userName = $_POST['userName'];
				$member -> SetPassword($_POST['password']);
				
				if($member -> Validate()) {
					$member -> Save();
				}
				
				break;			
		}
	}
	
	public function logout() {
		Session::destroy();
		$this -> redirect -> redirectPage(PATH . "login");
		exit();
	}


}
?>