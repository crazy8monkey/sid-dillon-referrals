<?php


class Index extends Controller {
		
	public function __construct() {	
		parent::__construct();
		Session::init();
		$profileLink = PATH . "member/dashboard";
		$loginLink = PATH . "login";
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == true) {
			$this -> view -> FooterMemberLink = $profileLink;
			$this -> view -> MemberLoginLink = $profileLink;
			$this -> view -> MemberLoginLinkText = "Your Profile";
			$this -> view -> FooterMemberText = "YOUR PROFILE";
			
		} else {
			$this -> view -> FooterMemberLink = $loginLink;
			$this -> view -> MemberLoginLink = $loginLink;
			$this -> view -> MemberLoginLinkText = "Member Login";
			$this -> view -> FooterMemberText = "MEMBER LOGIN";
		}
	}
	
	public function index() {
		$tiers = new ReferralTierList();
		
		$this -> view -> title = "Welcome" . parent::SIDDILLONTITLE;
		
		$this -> view -> ReferralSection = "";
		$this -> view -> referralTiers = $tiers -> getReferralTiers();
		$this -> view -> css = array('IndexController.css');
		$this -> view -> render('index/index');
	}


}
?>