<?php

class Rules extends Controller {
	
	public function __construct() {
		parent::__construct();
		
		Session::init();
		$profileLink = PATH . "member/dashboard";
		$loginLink = PATH . "login";
		$loggedIn = Session::get('UserLoggedIn');
		
		if($loggedIn == true) {
			$this -> view -> FooterMemberLink = $profileLink;
			$this -> view -> MemberLoginLink = $profileLink;
			$this -> view -> MemberLoginLinkText = "Your Profile";
			$this -> view -> FooterMemberText = "YOUR PROFILE";
		} else {
			$this -> view -> FooterMemberLink = $loginLink;
			$this -> view -> MemberLoginLink = $loginLink;
			$this -> view -> MemberLoginLinkText = "Member Login";
			$this -> view -> FooterMemberText = "MEMBER LOGIN";
		}
	}
	
	public function index() {
		$this -> view -> ReferralSection = "";
		$this -> view -> title = "Rules and Regulations" . parent::SIDDILLONTITLE;
		$this -> view -> pageTitle = "Rules and Regulations";
		
		$this -> view -> css = array('SubmitController.css');
		$this -> view -> render('rules/index');
	}
	
	
	



}
?>