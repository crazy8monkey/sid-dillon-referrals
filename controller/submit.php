<?php

class Submit extends Controller {
	
	private $_storesList;
	private $_salesmanList;
	
	private $_memberID;
	private $_isloggedIn;
	
	public function __construct() {
		
		parent::__construct();
		$this -> _salesmanList = new SalesmanList();
		
		Session::init();
		$profileLink = PATH . "member/dashboard";
		$loginLink = PATH . "login";
		$loggedIn = Session::get('UserLoggedIn');
		
		
		if($loggedIn == true) {
			$this -> _memberID = Session::get('user');
			$this -> _isloggedIn = 1;
			$this -> view -> FooterMemberLink = $profileLink;
			$this -> view -> MemberLoginLink = $profileLink;
			$this -> view -> MemberLoginLinkText = "Your Profile";
			$this -> view -> FooterMemberText = "YOUR PROFILE";
		} else {
			$this -> _memberID = '';
			$this -> _isloggedIn = 0;
			$this -> view -> FooterMemberLink = $loginLink;
			$this -> view -> MemberLoginLink = $loginLink;
			$this -> view -> MemberLoginLinkText = "Member Login";
			$this -> view -> FooterMemberText = "MEMBER LOGIN";
		}
	}
	
	public function index() {
		
		Session::init();
		$member = new ReferralMember();
		
		$loggedIn = Session::get('UserLoggedIn');
		
		if($loggedIn == true) {
			$referralMember = ReferralMember::WithID(Session::get('user'));
			$this -> view -> SDClubID = "SD" . $referralMember -> ClubID;
		} else {
			$this -> view -> SDClubID = "";
			$this -> view -> clubIDLookup = $member -> ClubIDLookupForm(PATH . "submit/save/clubIDlookup");
		}
		
		
		$this -> _storesList = new StoresList();		
		$this -> view -> title = "Submit A Referral" . parent::SIDDILLONTITLE;
		$this -> view -> ReferralSection = "";
		$this -> view -> pageTitle = "Submit A Referral";
		
		$this -> view -> js = array(PATH . "public/js/SubmitController.js");
		$this -> view -> startJsFunction = array('SubmitController.Initialize(); Globals.ValidateClubIDLookup("#clubID")');
		$this -> view -> css = array('SubmitController.css');
		$this -> view -> referralStores = $this -> _storesList -> getReferralStores();
		$this -> view -> render('submit/index');
	}
	
	public function GET($storeID) {
		echo json_encode($this -> _salesmanList -> getUsersByStore($storeID));
	}
	
	
	public function save($type) {
		switch($type) {
			case "referral":
				$referral = new ReferralSingle();
				$referral -> ClubID = $_POST['clubID'];
				$referral -> StoreID = $_POST['selectedStore'];
				$referral -> salesmanID = $_POST['salesman'];
				$referral -> FirstName = $_POST['firstName'];
				$referral -> LastName = $_POST['lastName'];
				$referral -> phone = $_POST['phoneInput'];
				$referral -> email = $_POST['email'];
				$referral -> desiredVehichle = $_POST['desiredVehicle'];
				$referral -> ExpectedPurchase = $_POST['ExpectedPurchase'];
				$referral -> ReferralNotes = $_POST['referralNotes'];

				$referral -> LoggedInMember = $this -> _isloggedIn;
				$referral -> LoggedInMemberID = $this -> _memberID;
				
				
				if(isset($_POST['SidDillonRelation'])) {
					$referral -> SidDillonRelation = $_POST['SidDillonRelation'];	
				}
				
				if($referral -> Validate()) {
					$referral -> Save();
				}
				
				break;
			case "clubIDlookup":
				$member = new ReferralMember();
				$member -> email = $_POST['clubIDlookupEmail'];
				
				$member -> getUserClubID();
				
				break;
		}
	}


}
?>