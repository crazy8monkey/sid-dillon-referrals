<?php


class FAQ extends Controller {
		
	public function __construct() {	
		parent::__construct();
		Session::init();
		$profileLink = PATH . "member/dashboard";
		$loginLink = PATH . "login";
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == true) {
			$this -> view -> FooterMemberLink = $profileLink;
			$this -> view -> MemberLoginLink = $profileLink;
			$this -> view -> MemberLoginLinkText = "Your Profile";
			$this -> view -> FooterMemberText = "YOUR PROFILE";
			
		} else {
			$this -> view -> FooterMemberLink = $loginLink;
			$this -> view -> MemberLoginLink = $loginLink;
			$this -> view -> MemberLoginLinkText = "Member Login";
			$this -> view -> FooterMemberText = "MEMBER LOGIN";
		}
	}
	
	public function index() {
		$this -> view -> title = "FAQ" . parent::SIDDILLONTITLE;
		
		$this -> view -> pageTitle = "Frequently Asked Questions";
		$this -> view -> css = array('FAQController.css');
		$this -> view -> ReferralSection = "";
		$this -> view -> render('faq/index');
	}


}
?>