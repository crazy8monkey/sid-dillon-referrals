<?php

class Register extends Controller {
	
	public function __construct() {
		parent::__construct();
		Session::init();
		$profileLink = PATH . "member/dashboard";
		$loginLink = PATH . "login";
		$loggedIn = Session::get('UserLoggedIn');
		if($loggedIn == true) {
			$this -> view -> FooterMemberLink = $profileLink;
			$this -> view -> MemberLoginLink = $profileLink;
			$this -> view -> MemberLoginLinkText = "Your Profile";
			$this -> view -> FooterMemberText = "YOUR PROFILE";
			
		} else {
			$this -> view -> FooterMemberLink = $loginLink;
			$this -> view -> MemberLoginLink = $loginLink;
			$this -> view -> MemberLoginLinkText = "Member Login";
			$this -> view -> FooterMemberText = "MEMBER LOGIN";
		}
	}
	
	public function index() {
		$member = new ReferralMember();
		$this -> view -> ReferralSection = "";
		$this -> view -> title = "Register" . parent::SIDDILLONTITLE;
		$this -> view -> pageTitle = "Register";
		$this -> view -> js = array(PATH . "public/js/RegisterController.js");
		$this -> view -> startJsFunction = array('RegisterController.Initialize();');
		$this -> view -> css = array('RegisterController.css');
		$this -> view -> states = $member -> States();
		$this -> view -> HideClosePoup = "";
		$this -> view -> render('register/index');
	}
	
	public function save($type) {
		switch($type) {
			case "register":
				
				$newMember = new ReferralMember();
				$newMember -> firstName = $_POST['firstName'];
				$newMember -> lastName = $_POST['lastName'];
				$newMember -> email = $_POST['email'];
				$newMember -> Phone = $_POST['phone'];
				$newMember -> address = $_POST['address'];
				$newMember -> city = $_POST['city'];
				$newMember -> State = $_POST['hiddenState'];
				$newMember -> zip = $_POST['Zip'];
				$newMember -> userName = $_POST['userName'];
				$newMember -> SetPassword($_POST['password']);
				
				if(isset($_POST['SidDillonRelation'])) {
					$newMember -> SidDillonRelation = $_POST['SidDillonRelation'];	
				}
				
				if(isset($_POST['agreement'])) {
					$newMember -> agreement = $_POST['agreement'];
				}
				
				if($newMember -> Validate()) {
					$newMember -> Save();
				}		
				break;
		}
		
		
		
		
	}


}
?>