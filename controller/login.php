<?php

class Login extends Controller {

	public function __construct() {
		parent::__construct();
		$this -> view -> memberLoginSection = "";
		$this -> view -> css = array('MemberController.css');
		$this -> view -> js = array(PATH . "public/js/LoginController.js");
		$this -> view -> ReferralMainText = "Referral Program Login";
		$this -> view -> ReferralMainLink = PATH . "login";
	}
			
	public function index() {
		$this -> view -> title = "Member Login" . parent::SIDDILLONTITLE;
		$this -> view -> startJsFunction = array('LoginController.InitializeLogin();');
		$this -> view -> css = array('MemberController.css');
		$this -> view -> render('login/login');
	}
	
	
	
	public function credentials($type) {
		$this -> view -> memberLoginSection = "";	
		$this -> view -> css = array('MemberController.css');
			
		switch($type) {
			case "create":
				$member = new ReferralMember();
				$this -> view -> title = "Create Your Credentials" . parent::SIDDILLONTITLE;
				$this -> view -> clubIDLookup = $member -> ClubIDLookupForm(PATH . "login/save/clubIDlookup");
				
				$this -> view -> startJsFunction = array('LoginController.InitializeCreateCredentials(); Globals.ValidateClubIDLookup("#userClubID")');
				$this -> view -> render('login/create');
				break;
			case "forgot":
				$this -> view -> title = "Forgot Your Credentials" . parent::SIDDILLONTITLE;
				$this -> view -> startJsFunction = array('LoginController.InitializeForgotCredentials();');
				$this -> view -> render('login/forgot');
				break;
			case "reset":
				$this -> view -> title = "Reset Your Credentials" . parent::SIDDILLONTITLE;
				$this -> view -> startJsFunction = array('LoginController.InitializeReset();');
				$this -> view -> userToken = ReferralMember::WithID($_GET['u']);
				$this -> view -> render('login/reset');
				break;
		}
	}
	
	
	
	
	public function save($type, $id = NULL) {
		switch($type) {
			case "credentials":
				$member = new ReferralMember();
				
				$member -> ClubID = $_POST['userClubID'];			
				$member -> userName = $_POST['newUsername'];
				$member -> SetPassword($_POST['newPassword']);
				if($member -> ValidateCredentials()) {
					$member -> SaveCredentials();
				}
				
				
				break;
			
			case "userlogin":
				$memberLogin = new ReferralMember();
				$memberLogin -> userName = $_POST['username'];
				$memberLogin -> password = $_POST['password'];
				
				$memberLogin -> userLogin();
				break;
			case "clubIDlookup":
				$member = new ReferralMember();
				$member -> email = $_POST['clubIDlookupEmail'];
				
				$member -> getUserClubID();
				
				break;
				
			case "forgotcredentials":
				$memberLogin = new ReferralMember();
				$memberLogin -> email = $_POST['userEmailCheck'];
				
				$memberLogin -> SendEmail();
				break;
			case "password":
				$memberLogin = ReferralMember::WithID($id);
				$memberLogin -> SetPassword($_POST['userNewPassword']);
				if($memberLogin -> ValidateNewPassword()) {
					$memberLogin -> SavePassword();
				}
				
				break;
			
		}
	}


}
?>